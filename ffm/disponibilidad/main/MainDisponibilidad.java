package ffm.disponibilidad.main;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import ffm.disponibilidad.bi.DisponibilidadBI;
import ffm.disponibilidad.util.Constantes;
import ffm.disponibilidad.util.IMensajes;
import ffm.disponibilidad.vo.ArgumentosVO;
import ffm.disponibilidad.vo.GeocercaQVO;
import ffm.disponibilidad.vo.GeocercaVO;
import ffm.disponibilidad.vo.ParametrosVO;
import ffm.disponibilidad.vo.TurnoVO;

public class MainDisponibilidad {
	private static ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring_config.xml");
	private static DisponibilidadBI disponibilidadBI = (DisponibilidadBI) applicationContext.getBean("disponibilidadBI");
	private static IMensajes mensajes = (IMensajes) applicationContext.getBean("mensajes");
	private static List<GeocercaVO> geocercas = new ArrayList<GeocercaVO>();
	private static List<GeocercaQVO> listGeocercasQVO = new ArrayList<GeocercaQVO>();
	private static List<ParametrosVO> parameters = new ArrayList<ParametrosVO>();
	private static List<TurnoVO> listTurnos = new ArrayList<TurnoVO>();
	private static ArgumentosVO argumentsVO = new ArgumentosVO();
	
	public static void main(String[] args) {
		System.out.println(Constantes.US);
		getArguments(args);
		getParameters(argumentsVO);
		/**************************************************************************************************/
		getGeocercas();
		
		getListGeocercasQ();
		
		getTurns();
		/**************************************************************************************************/
		while(processQueue() == 1) {
			getParameters(argumentsVO);
			getListGeocercasQ();
			getGeocercas();
			getTurns();
		}
	}
	
	private static void getArguments(String[] args) {
		try {
			mensajes.console("main", "Inicia obtener argumentos");
			argumentsVO = disponibilidadBI.setArguments(args);
		}catch(Exception e) {
			mensajes.console("Codigo error: 001", "Error: Algo salio mal al ejecutar getArguments exception: " + e.getMessage());
		}
	}
	
	private static void getParameters(ArgumentosVO argumentsVO) {
		try {
			mensajes.console("main", "Inicia obtener parametros");
			parameters = disponibilidadBI.getParameters(argumentsVO);
		} catch (Exception e) {
			mensajes.console("main", "Algo salio mal al ejecutar processBI.getParameters exception: " + e.getMessage());
		}
	}
	
	private static void getGeocercas() {
		try {
			mensajes.console("main", "Inicia obtener geocercas");
			geocercas = disponibilidadBI.getGeocercas(parameters);
		} catch (Exception e) {
			mensajes.console("main", "Algo salio mal al ejecutar processBI.getGeocercas exception: " + e.getMessage());
		}
	}
	
	private static void getListGeocercasQ() {
		try {
			mensajes.console("main", "Inicia obtener geocercasQueue");
			listGeocercasQVO = disponibilidadBI.getGeocercasQ(parameters);
		} catch (Exception e) {
			mensajes.console("main", "Algo salio mal al ejecutar processBI.getListGeocercasQ exception: " + e.getMessage());
		}
	}
	
	private static void getTurns() {
		try {
			mensajes.console("main", "inicia obtener Turnos");	
			listTurnos = disponibilidadBI.getTurns(parameters);
		}catch(Exception e) {
			mensajes.console("main", "Algo salio mal al ejecutar getTurns");
		}
	}
	
	private static int processQueue() {
		int resetParameters = 0;
		try {
			mensajes.console("main", "Inicia proceso Queue");
			resetParameters = disponibilidadBI.process(listGeocercasQVO,argumentsVO,parameters,geocercas,listTurnos);
		}catch(Exception e) {
			mensajes.console("main", "Algo salio mal al ejecutar proceso: " + e.getMessage());
		}
		return resetParameters;
	}
}