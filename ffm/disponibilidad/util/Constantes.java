package ffm.disponibilidad.util;

public class Constantes {
	public static final String MODULO = "FFMDISPONIBILIDAD";
	public static final String SISTEMA = "FFM";
	public static final String VERSION = "2.2";
	public static final int FAPID_GETPARAMETERS = 0;
	public static final int RANGOEJECUCION = FAPID_GETPARAMETERS;		
	public static final int UPDATERESETPARAMETERS = 7;
	public static final int FAPID_RESETPARAMETERS = 10;	
	public static final int FAPID_QTTIMEZONE = 20002;
	public static final int FAPID_SKILLSTIEMPOPROMEDIO = 82000;
	public static final int FAPID_OTS = 83000;
	public static final int FAPID_JORNADAS = 83001;
	public static final int FAPID_HOUROTASSIGN= 20500;
	public static final int FAPID_CUADRILLAS = 20501;
	public static final int FAPID_CLEARFFMQUEUE= 20502;
	public static final int FAPID_INSERTFFMQUEUE= 20503;
	public static final int FAPID_QTURN = 20504;	
	public static final int qFAPID_INSERTFFMAAEVENTS = 20505;
	public static final int NUMBERBUCLES = 11;
	public static final int VALIDALOCK = 3;
	public static final int LOCKUNLOCK = 4;	
	public static final int INFINITO = 999999;
	public static final int SLEEPTIME = 6;	
	public static final int FAPSTATUS = 1;	
	public static final int QGETSEMAFORO = 5;
	public static final int qTimeZone = 20002;
	public static String US;
	public static String UR;
}
