package ffm.disponibilidad.util;

import java.time.LocalTime;

import org.springframework.stereotype.Component;
@Component("mensajes")
public class MensajesImpl implements IMensajes{

	@Override
	public void console(String rutina, String mensaje) {
		String straux="";
        straux += LocalTime.now()     + ",";
        straux += Constantes.SISTEMA  + ",";
        straux += Constantes.MODULO   + ",";
        straux += Constantes.VERSION  + "," ;
        straux += "Rutina: "+ rutina  + "," ;
        straux += mensaje ;
        System.out.println(straux); 		
	}

}
