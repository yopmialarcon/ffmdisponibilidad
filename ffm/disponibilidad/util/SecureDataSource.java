package ffm.disponibilidad.util;

import org.apache.commons.dbcp.BasicDataSource;

public class SecureDataSource extends BasicDataSource {

	public SecureDataSource() {
	}
	
	private String env = System.getenv().get("FFM_AA_01");

	public synchronized void setPs(String encodedPassword) {
		try {
			String[] straux = encodedPassword.replace(",", ";").split(";");
			byte[] p = new byte[straux.length];
			if (p != null)
				if (!encodedPassword.isEmpty()) {
					for (int i = 0; i < straux.length; i++) {
						p[i] = (byte) (Integer.parseInt(straux[i]));
					}
				}
			this.password = Crypto.decrypt(p, env);
		} catch (Exception e) {
			System.out.println("Ocurrio un error al decodificar pwd bd: "+ e.getMessage());
		}
	}
	
	public synchronized void setUs(String encodedUsername) {
		try {
			String[] straux = encodedUsername.replace(",", ";").split(";");
			byte[] u = new byte[straux.length];
			if (u != null)
				if (!encodedUsername.isEmpty()) {
					for (int i = 0; i < straux.length; i++) {
						u[i] = (byte) (Integer.parseInt(straux[i]));
					}
				}
			this.username = Crypto.decrypt(u, env);
			Constantes.US = this.username;
		} catch (Exception e) {
			System.out.println("Ocurrio un error al decodificar usuario db: "+ e.getMessage());
		}
	}
	
	public synchronized void setUrl(String encodedUrl) {
		try {
			String[] straux = encodedUrl.replace(",", ";").split(";");
			byte[] ur = new byte[straux.length];
			if (ur != null)
				if (!encodedUrl.isEmpty()) {
					for (int i = 0; i < straux.length; i++) {
						ur[i] = (byte) (Integer.parseInt(straux[i]));
					}
				}
			this.url = Crypto.decrypt(ur, env);
			Constantes.UR = this.url;
		} catch (Exception e) {
			System.out.println("Ocurrio un error al decodificar url: "+ e.getMessage());
		}
	}
}
