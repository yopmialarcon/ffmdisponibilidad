package ffm.disponibilidad.bi;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ffm.disponibilidad.dao.DisponibilidadDAO;
import ffm.disponibilidad.entidades.FFM_AA_EVENTOS;
import ffm.disponibilidad.entidades.FFM_QUEUE_THEORY;
import ffm.disponibilidad.util.Constantes;
import ffm.disponibilidad.util.IMensajes;
import ffm.disponibilidad.vo.ArgumentosVO;
import ffm.disponibilidad.vo.CuadrillasVO;
import ffm.disponibilidad.vo.GeocercaQVO;
import ffm.disponibilidad.vo.GeocercaVO;
import ffm.disponibilidad.vo.OTVO;
import ffm.disponibilidad.vo.ParametrosVO;
import ffm.disponibilidad.vo.TurnoVO;

@Component("disponibilidadBI")
public class DisponibilidadBImpl implements DisponibilidadBI{

	@Autowired
	private DisponibilidadDAO disponibilidadDAO;
	@Autowired
	private IMensajes mensajes;
	
	@Override
	public ArgumentosVO setArguments(String[] args) {
		try {
			if (args != null) {
				ArgumentosVO arguments = new ArgumentosVO();
				arguments.setConfiguracion(Integer.parseInt(args[0]));	
				return arguments;
			} else {//test
				mensajes.console("Argumentos", "Algo salio mal, los argumentos vienen vacios");
				return null;
			}
		} catch (Exception e) {
			mensajes.console("Argumentos", "Algo salio mal, exception: " + e.getMessage());
			return null;
		}
	}

	@Override
	public List<ParametrosVO> getParameters(ArgumentosVO argumentsVO) {
		try {
				List<ParametrosVO> listParametrosVO = disponibilidadDAO.getParameters(argumentsVO);
				if(listParametrosVO != null) {	
					mensajes.console("PARAMETROS: ", "*******************************************************************************************************************************************");
					for (ParametrosVO parametrosVO : listParametrosVO) {		
						//Print Parameters						
						mensajes.console("PARAMETROS: ", parametrosVO.getFapId() +": "+ parametrosVO.getFapStatus()+ ": " +parametrosVO.getFapValor());										
					}
					mensajes.console("PARAMETROS: ", "*******************************************************************************************************************************************");
					return listParametrosVO;
				}else {
					mensajes.console("PARAMETROS", "Algo salio mal la cadena de geocercas esta null");
					return null;
				}	
		} catch (Exception e) {
			mensajes.console("PARAMETROS", "Algo salio mal exception: " + e.getMessage());
			return null;
		}
	}

	@Override
	public List<GeocercaVO> getGeocercas(List<ParametrosVO> listParametrosVO) {
		try {
			List<GeocercaVO> listGeocercaVO= new ArrayList<>();
			if(listParametrosVO != null) {	
				String query = "";
				for (ParametrosVO parametrosVO : listParametrosVO) {
					if (parametrosVO.getFapId() == Constantes.qTimeZone) {
						query = parametrosVO.getFapValor();
						break;
					}
				}
				for (ParametrosVO parametrosVO : listParametrosVO) {	
					String[] splitParameters = parametrosVO.getFapValor().split(";");
					if(splitParameters[0].toUpperCase().indexOf("GEOCERCA_ID") != -1) {
						GeocercaVO geocerca = new GeocercaVO();
						String[] idGeocerca = splitParameters[0].split(",");
						String[] isActive = splitParameters[1].split(",");
						String[] startTime = splitParameters[2].split(",");
						String[] endTime = splitParameters[3].split(",");
						if(idGeocerca[1] != null) {
							geocerca.setId(Integer.parseInt(idGeocerca[1]));
							//GetTimeZone
							if(validaS(query)) {
								disponibilidadDAO.getTimeZone(query, geocerca,null);
							}else {
								mensajes.console("validaS", "El query no paso la prueba de validacion, se aborta proceso");
								return null;
							}
							
							if(geocerca.getTimeZone() == null || geocerca.getTimeZone().isEmpty()) {
								mensajes.console("Geocercas","Algo salio mal al obtener timezone de geocerca: "+ geocerca.getId());	
							}						
						} else {
							mensajes.console("Geocercas", "Algo salio mal no se encuentra id de geocerca");
						}
						if(isActive[1] != null) {
							geocerca.setEnabled(isActive[1].equals("1") ? true : false);
						} else {
							mensajes.console("Geocercas", "Algo salio mal no se encuentra bandera de geocerca " +geocerca.getId());
						}
						if(startTime[1] != null) {
							geocerca.setStartTime(startTime[1]);
						} else {
							mensajes.console("Geocercas", "Algo salio mal no se encuentra hora de inicio geocerca "+ geocerca.getId());
						}
						if(endTime[1] != null) {
							geocerca.setEndTime(endTime[1]);
						} else {
							mensajes.console("Geocercas", "Algo salio mal no se encuentra hora de fin geocerca "+ geocerca.getId());
						}
						if(isActive[1] != null) {
							geocerca.setEnabled(isActive[1].equals("1") ? true : false);
						} else {
							mensajes.console("Geocercas", "Algo salio mal no se encuentra bandera de geocerca "+geocerca.getId());
						}
						listGeocercaVO.add(geocerca);
						parametrosVO.setGeocerca(true);				
					} else {
						parametrosVO.setGeocerca(false);
					}
				}
			}else {
				mensajes.console("Lista ParametrosVO", "Se encuentra null");
			}
			return listGeocercaVO;
		}catch(Exception e){
			mensajes.console("getGeocercas", "Algo salio mal exception: " + e.getMessage());
			return null;
		}
		
		
	}
	
	@Override
	public List<GeocercaQVO> getGeocercasQ(List<ParametrosVO> listParametersVO) {
		
		List<GeocercaQVO> listGeocercaQVO = new ArrayList<GeocercaQVO>();
		String qTimeZone = "";
		String qOTs = "";
		String qCuadrillas = "";
		String qJornadas = "";
		String qHourOtAssign = "";
		String skillsTiempoPromedio = "";
		String qClearFfmQueue = "";
		String qInsertFfmQueue = "";
		String qInsertFAE = "";
		
		try {
			for (ParametrosVO parametrosVO : listParametersVO) {
				if (parametrosVO.getFapId() == Constantes.FAPID_QTTIMEZONE) {
					qTimeZone = parametrosVO.getFapValor();
				}
				if (parametrosVO.getFapId() == Constantes.FAPID_SKILLSTIEMPOPROMEDIO) {
					skillsTiempoPromedio = parametrosVO.getFapValor();
				}
				if (parametrosVO.getFapId() == Constantes.FAPID_OTS) {
					String[] parametro = parametrosVO.getFapValor().split("SEPARADOR");
					qOTs = parametro[1];
				}
				if (parametrosVO.getFapId() == Constantes.FAPID_JORNADAS) {
					String[] parametro = parametrosVO.getFapValor().split("SEPARADOR");
					qJornadas = parametro[1];
				}
				if (parametrosVO.getFapId() == Constantes.FAPID_HOUROTASSIGN) {
					String[] parametro = parametrosVO.getFapValor().split("SEPARADOR");
					qHourOtAssign = parametro[1];
				}
				if (parametrosVO.getFapId() == Constantes.FAPID_CUADRILLAS) {
					String[] parametro = parametrosVO.getFapValor().split("SEPARADOR");
					qCuadrillas = parametro[1];
				}
				if (parametrosVO.getFapId() == Constantes.FAPID_CLEARFFMQUEUE) {
					String[] parametro = parametrosVO.getFapValor().split("SEPARADOR");
					qClearFfmQueue = parametro[1];
				}
				if (parametrosVO.getFapId() == Constantes.FAPID_INSERTFFMQUEUE) {
					String[] parametro = parametrosVO.getFapValor().split("SEPARADOR");
					qInsertFfmQueue = parametro[1];
				}if (parametrosVO.getFapId() == Constantes.qFAPID_INSERTFFMAAEVENTS) {
					qInsertFAE = parametrosVO.getFapValor();
				}
				
			}
			
			for (ParametrosVO parametrosVO : listParametersVO) {
				String[] splitParameters = parametrosVO.getFapValor().split(";");
				
				if(splitParameters[0].equals("Google,0") || splitParameters[0].equals("Google,1")) {
					GeocercaQVO geocercaQVO = new GeocercaQVO();
					geocercaQVO.setId(Integer.parseInt(String.valueOf(parametrosVO.getFapStatus())));
					geocercaQVO.setqOTs(qOTs);
					geocercaQVO.setqCuadrillas(qCuadrillas);
					geocercaQVO.setqJornada(qJornadas);
					geocercaQVO.setqHourOtAssign(qHourOtAssign);
					geocercaQVO.setqClearFfmQueue(qClearFfmQueue);
					geocercaQVO.setSkillsTiempoPromedio(skillsTiempoPromedio);
					geocercaQVO.setqInsertFfmQueue(qInsertFfmQueue);
					geocercaQVO.setqInsertFAE(qInsertFAE);
					for (int i = 0; i < splitParameters.length; i++) {
						String[] x = splitParameters[i].split(",");
						switch(x[0].toUpperCase()) {
							case "GOOGLE":							
								geocercaQVO.setGoogle(Integer.parseInt(x[1]));
								break;
							case "TIEMPOPORSKILL":							
								geocercaQVO.setTimeBySkill(Integer.parseInt(x[1]));
								break;
							case "HABILITADA":							
								geocercaQVO.setEneable(Integer.parseInt(x[1]));
								break;
							case "SKILLS":								
								geocercaQVO.setSkills(splitParameters[3].substring(7, splitParameters[3].length()));
								break;
							case "HORARIO":							
								geocercaQVO.setStartTime(x[1]);
								geocercaQVO.setEndTime(x[2]);
								break;
							case "TOLERANCIA":							
								geocercaQVO.setTolerance(Integer.parseInt(x[1]));
								break;
							case "VELOCIDAD":							
								geocercaQVO.setSpeed(Integer.parseInt(x[1]));
								break;
							case "TURNO":							
								geocercaQVO.setTurn(Integer.parseInt(x[1]));
								break;
							case "MINUTOSESPERARESERVADASIVR":							
								geocercaQVO.setMinutesWaitIVR(Integer.parseInt(x[1]));
								break;
							case "OTS_ANEJAS":							
								geocercaQVO.setoTsOld(Integer.parseInt(x[1]));
								break;
							case "OTS_PREMATURAS":							
								geocercaQVO.setoTsPrematures(Integer.parseInt(x[1]));
								break;
						}
					} 
					disponibilidadDAO.getTimeZone(qTimeZone, null, geocercaQVO);
					listGeocercaQVO.add(geocercaQVO);
				}
			}
		}catch(Exception e) {
			mensajes.console("getGeocercasQ", "Algo salio mal exception: " + e.getMessage());
			for (Object o : e.getStackTrace()) {
				mensajes.console("getGeocercasQ", "Algo salio mal exception: " + o.toString());		
			}		
		}		
		return listGeocercaQVO;
	}	
	
	@Override
	public int process(List<GeocercaQVO> listGeocercasQVO,ArgumentosVO argumentsVO,List<ParametrosVO> listParametersVO, List<GeocercaVO> listGeocercas, List<TurnoVO> listTurnos) {
		int resetParameters = 0;
		try {
			String queryUpdateResetParams = "";	
			int parameterBucle = 0;
			float startHour = 0;
			float endHour = 0;
			int sleepTime = 0;		
			float now = 0;
			int bucle = 1;
			
			for (ParametrosVO parametrosVO : listParametersVO) {			
				
				switch (parametrosVO.getFapId() - argumentsVO.getConfiguracion()) {
				case Constantes.RANGOEJECUCION:
					String[] schedule = parametrosVO.getFapValor().split(",");
					startHour = parseHour(schedule[1]);
					endHour = parseHour(schedule[2]);
					now = parseHour("" + LocalTime.now());
					break;
				case Constantes.SLEEPTIME:
					sleepTime = Integer.parseInt(parametrosVO.getFapValor());
					break;
				case Constantes.NUMBERBUCLES:
					String[] param = parametrosVO.getFapValor().split(",");
					parameterBucle = Integer.parseInt(param[1]);
					break;
				case Constantes.UPDATERESETPARAMETERS:
					String[] parametro = parametrosVO.getFapValor().split("\\|");
					queryUpdateResetParams = parametro[1];
				break;
				}				
			}
			parameterBucle = parameterBucle == 0 ? Constantes.INFINITO : parameterBucle;
			while (now > startHour && now < endHour && resetParameters == 0 && bucle <= parameterBucle) {
				for (GeocercaQVO geocercaQVO : listGeocercasQVO) {
					
					String localTime =""+LocalTime.now().plusHours(geocercaQVO.getHoursDifference()); 
					if(parseHour(localTime) > parseHour(geocercaQVO.getStartTime()) && parseHour(localTime) < parseHour(geocercaQVO.getEndTime())) {
						//get OTs
						List<OTVO> listOTVO = new ArrayList<OTVO>();
//						if(Constantes.US.equals("FFMTPE")){
//							listOTVO = disponibilidadDAO.getListOTsEmp(geocercaQVO);
//						}else if(Constantes.US.equals("TPASAPSEC")) {
//							listOTVO = disponibilidadDAO.getListOTs(geocercaQVO);
//						}
						listOTVO = disponibilidadDAO.getListOTs(geocercaQVO);
						for (OTVO otvo : listOTVO) {
							otvo.setTimeAvg(setTiempoPromedio(otvo,geocercaQVO.getSkillsTiempoPromedio()));
						}
						
						geocercaQVO.setListOTs(listOTVO);
						
						//get Cuadrillas
						List<CuadrillasVO> listCuadrillasVO = new ArrayList<>();
						listCuadrillasVO = disponibilidadDAO.getListCuadrillas(geocercaQVO);
												
						geocercaQVO.setListCuadrillasVO(listCuadrillasVO);
					}else {
						mensajes.console("Ejecuta Queue proceso", "La geocerca: "+ geocercaQVO.getId() +" se encuentra fuera de horario.");
					}					
				}
				processQueue(listGeocercasQVO,listTurnos);
				exeLockUnlock(listParametersVO, listGeocercas, argumentsVO);					
				
				String m = parameterBucle == Constantes.INFINITO ? " infinito" : String.valueOf(parameterBucle);
				mensajes.console("Ejecuta Queue proceso", "Numero de ciclos: "+ bucle +" de: "+ m );
				bucle++;
				mensajes.console("Ejecuta Queue proceso", "Sleep "+ ((sleepTime/1000) / 60) +" minutos");
				Thread.sleep(sleepTime);
				now = parseHour("" + LocalTime.now());
				resetParameters = disponibilidadDAO.getResetParameters(argumentsVO);
				mensajes.console("Ejecuta Queue proceso", "Parametro reset parametros de configuracion: "+ resetParameters);
			}		
			
			if(resetParameters == 0) {
				if(parameterBucle == Constantes.INFINITO) {
					mensajes.console("Ejecuta Ejecuta Queue proceso", "Ejecucion fuera de horario.");
				}else {
					mensajes.console("Ejecuta Ejecuta Queue proceso", "Ciclos completos.");
				}
				
			}else if(resetParameters == 1) {
				disponibilidadDAO.updateResetParameters(queryUpdateResetParams,argumentsVO);
				mensajes.console("Ejecuta Queue proceso", "Reset de parametros.");
			}else if(resetParameters == -1) {
				mensajes.console("Ejecuta Ejecuta Queue proceso", "Ocurrio un error al obtener el parametro para reset de parametros de configuracion");
			}			
						  
		}catch(Exception e) {
			for (Object o : e.getStackTrace()) {
				mensajes.console("process", "Algo salio mal exception: " + o.toString());		
			}
		}
		return resetParameters;
	}

	@Override
	public List<TurnoVO> getTurns(List<ParametrosVO> parameters) {
		String qGetTurn = "";
		for (ParametrosVO parametrosVO : parameters) {
			if (parametrosVO.getFapId() == Constantes.FAPID_QTURN) {
				qGetTurn = parametrosVO.getFapValor();
				break;
			}
		}
		return disponibilidadDAO.findAllFfmCatTurn(qGetTurn);
	}	
	
	//Metodos locales

	@SuppressWarnings("unchecked")
	private void processQueue(List<GeocercaQVO> listGeocercasQVO, List<TurnoVO> listTurnos) {		
		try {
			//Get number day of week
			DateFormat dateFormat = new SimpleDateFormat("yyyy|MM|dd");
			Calendar c = Calendar.getInstance();
			Date date = new Date();
			String[] d = dateFormat.format(date).split("\\|");			 
			c.set(Integer.parseInt(d[0]),Integer.parseInt(d[1])-1,Integer.parseInt(d[2]));
			int diaSemana = c.get(Calendar.DAY_OF_WEEK);
			//End Get number day of week
			
			for (GeocercaQVO geocercaQVO : listGeocercasQVO) {
				
				//Pull apart OTS
				List<OTVO> listOTsHour = new ArrayList<OTVO>();
				List<OTVO> listOTsEmp = new ArrayList<OTVO>();
				List<OTVO> listOTsSop = new ArrayList<OTVO>();
				List<OTVO> listOTsResultMat = new ArrayList<OTVO>();
				List<OTVO> listOTsResultVesp = new ArrayList<OTVO>();
				
				if(geocercaQVO.getListOTs() != null) {
					for (OTVO oTVO : geocercaQVO.getListOTs()) {
						if(oTVO.getFotHour() != null) {
							listOTsHour.add(oTVO);
						}else if(oTVO.getFciIdSubTypeInter() == 89){
							listOTsEmp.add(oTVO);
						}else {
							listOTsSop.add(oTVO);
						}
					}
				}else {
					mensajes.console("processQueue","La geocerca: "+ geocercaQVO.getId() +" no cuenta con OTs");
				}
				
				//End Pull apart OTS
				
				String localT =""+LocalTime.now().plusHours(geocercaQVO.getHoursDifference()); 
				float nowHour = parseHour(localT);				
				
				List<CuadrillasVO> listCuadrillas = new ArrayList<CuadrillasVO>();
				float bolsaMinutosCuadrillas = 0;
				
				if(geocercaQVO.getListCuadrillasVO() != null) {
					for (int j = 0; j<geocercaQVO.getListCuadrillasVO().size();j++) {
						List<FFM_AA_EVENTOS> listFFM_AA_EVENTOS = new ArrayList<FFM_AA_EVENTOS>();
						FFM_AA_EVENTOS fae = new FFM_AA_EVENTOS();
						float minutosRestantes = 0;
						Date jornadaInicial = null;
						Date jornadaFinal = null;
						
						switch(diaSemana) {
						 case 1: 
							 fae = new FFM_AA_EVENTOS();
							 if(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO() != null && (geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getdInicia() != null || geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getdTermina() != null)) {
								 if(nowHour > parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getdInicia()) && nowHour < parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getdTermina())) {
									 jornadaInicial = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getdInicia());
									 jornadaFinal = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getdTermina());
									 listCuadrillas.add(geocercaQVO.getListCuadrillasVO().get(j));								 
								 } 
							 }else {
								 fae.setFAE_TIMESTAMP(LocalDateTime.now());
								 fae.setFAE_MODULO(Constantes.MODULO+","+Constantes.VERSION);
								 fae.setFAE_PROCESO(Constantes.MODULO);
								 fae.setFAE_FFO_FCT_ID(geocercaQVO.getId());
								 fae.setFAE_FFO_ID(geocercaQVO.getListCuadrillasVO().get(j).getFfoId());
								 fae.setFAE_MENSAJE("El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
								 listFFM_AA_EVENTOS.add(fae);
								 mensajes.console("Switch Asigna Jornadas", "El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
							 }
							 
							 break;
						 case 2: 
							 fae = new FFM_AA_EVENTOS();
							 if(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO() != null && (geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getlInicia() != null || geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getlTermina() != null)) {
								 if(nowHour > parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getlInicia()) && nowHour < parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getlTermina())) {
									 jornadaInicial = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getlInicia());
									 jornadaFinal = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getlTermina());
									 listCuadrillas.add(geocercaQVO.getListCuadrillasVO().get(j));
								 }	 
							 }else {
								 fae.setFAE_TIMESTAMP(LocalDateTime.now());
								 fae.setFAE_MODULO(Constantes.MODULO+","+Constantes.VERSION);
								 fae.setFAE_PROCESO(Constantes.MODULO);
								 fae.setFAE_FFO_FCT_ID(geocercaQVO.getId());
								 fae.setFAE_FFO_ID(geocercaQVO.getListCuadrillasVO().get(j).getFfoId());
								 fae.setFAE_MENSAJE("El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
								 listFFM_AA_EVENTOS.add(fae);
								 mensajes.console("Switch Asigna Jornadas", "El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
							 }						 						 
							 break;
						 case 3: 
							 fae = new FFM_AA_EVENTOS();
							 if(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO() != null && (geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getmInicia() != null || geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getmTermina() != null)) {
								 if(nowHour > parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getmInicia()) && nowHour < parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getmTermina())) {
									 jornadaInicial = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getmInicia());
									 jornadaFinal = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getmTermina());
									 listCuadrillas.add(geocercaQVO.getListCuadrillasVO().get(j));
								 }	 
							 }else {
								 fae.setFAE_TIMESTAMP(LocalDateTime.now());
								 fae.setFAE_MODULO(Constantes.MODULO+","+Constantes.VERSION);
								 fae.setFAE_PROCESO(Constantes.MODULO);
								 fae.setFAE_FFO_FCT_ID(geocercaQVO.getId());
								 fae.setFAE_FFO_ID(geocercaQVO.getListCuadrillasVO().get(j).getFfoId());
								 fae.setFAE_MENSAJE("El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
								 listFFM_AA_EVENTOS.add(fae);
								 mensajes.console("Switch Asigna Jornadas", "El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
							 }	
							 
							 break;
						 case 4:
							 fae = new FFM_AA_EVENTOS();
							 if(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO() != null && (geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getwInicia() != null || geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getwTermina() != null)) {
								 if(nowHour > parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getwInicia()) && nowHour < parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getwTermina())) {
									 jornadaInicial = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getwInicia());
									 jornadaFinal = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getwTermina());
									 listCuadrillas.add(geocercaQVO.getListCuadrillasVO().get(j));
								 } 
							 }else {
								 fae.setFAE_TIMESTAMP(LocalDateTime.now());
								 fae.setFAE_MODULO(Constantes.MODULO+","+Constantes.VERSION);
								 fae.setFAE_PROCESO(Constantes.MODULO);
								 fae.setFAE_FFO_FCT_ID(geocercaQVO.getId());
								 fae.setFAE_FFO_ID(geocercaQVO.getListCuadrillasVO().get(j).getFfoId());
								 fae.setFAE_MENSAJE("El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
								 listFFM_AA_EVENTOS.add(fae);
								 mensajes.console("Asigna Jornadas", "El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
							 }
							 
							 break;
						 case 5: 
							 fae = new FFM_AA_EVENTOS();
							 if(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO() != null && (geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getjInicia() != null || geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getjTermina() != null)) {
								 if(nowHour > parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getjInicia()) && nowHour < parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getjTermina())) {
									 jornadaInicial = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getjInicia());
									 jornadaFinal = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getjTermina());
									 listCuadrillas.add(geocercaQVO.getListCuadrillasVO().get(j));
								 }
							 }else {
								 fae.setFAE_TIMESTAMP(LocalDateTime.now());
								 fae.setFAE_MODULO(Constantes.MODULO+","+Constantes.VERSION);
								 fae.setFAE_PROCESO(Constantes.MODULO);
								 fae.setFAE_FFO_FCT_ID(geocercaQVO.getId());
								 fae.setFAE_FFO_ID(geocercaQVO.getListCuadrillasVO().get(j).getFfoId());
								 fae.setFAE_MENSAJE("El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
								 listFFM_AA_EVENTOS.add(fae);
								 mensajes.console("Switch Asigna Jornadas", "El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
							 }
							 
							 break;
						 case 6: 
							 fae = new FFM_AA_EVENTOS();
							 if(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO() != null && (geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getvInicia() != null || geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getvTermina() != null)) {
								 if(nowHour > parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getvInicia()) && nowHour < parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getvTermina())) {
									 jornadaInicial = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getvInicia());
									 jornadaFinal = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getvTermina());
									 listCuadrillas.add(geocercaQVO.getListCuadrillasVO().get(j));
								 }
							 }else {
								 fae.setFAE_TIMESTAMP(LocalDateTime.now());
								 fae.setFAE_MODULO(Constantes.MODULO+","+Constantes.VERSION);
								 fae.setFAE_PROCESO(Constantes.MODULO);
								 fae.setFAE_FFO_FCT_ID(geocercaQVO.getId());
								 fae.setFAE_FFO_ID(geocercaQVO.getListCuadrillasVO().get(j).getFfoId());
								 fae.setFAE_MENSAJE("El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
								 listFFM_AA_EVENTOS.add(fae);
								 mensajes.console("Switch Asigna Jornadas", "El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
							 }
							 
							 
							 break;
						 case 7: 
							 fae = new FFM_AA_EVENTOS();
							 if(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO() != null && (geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getsInicia() != null || geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getsTermina() != null)) {
								 if(nowHour > parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getsInicia()) && nowHour < parseHour(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getsTermina())) {
									 jornadaInicial = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getsInicia());
									 jornadaFinal = parseHourDate(geocercaQVO.getListCuadrillasVO().get(j).getJornadaVO().getsTermina());
									 listCuadrillas.add(geocercaQVO.getListCuadrillasVO().get(j));
								 }
							 }else {
								 fae.setFAE_TIMESTAMP(LocalDateTime.now());
								 fae.setFAE_MODULO(Constantes.MODULO+","+Constantes.VERSION);
								 fae.setFAE_PROCESO(Constantes.MODULO);
								 fae.setFAE_FFO_FCT_ID(geocercaQVO.getId());
								 fae.setFAE_FFO_ID(geocercaQVO.getListCuadrillasVO().get(j).getFfoId());
								 fae.setFAE_MENSAJE("EL TECNICO : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" NO CUENTA CON JORNADA PARA ESTE DIA");
								 listFFM_AA_EVENTOS.add(fae);
								 mensajes.console("Switch Asigna Jornadas", "El tecnico : "+ geocercaQVO.getListCuadrillasVO().get(j).getFfoId() +" no cuenta con jornada para este dia.");
							 }						 
							 break;
					 }
						if(jornadaInicial != null && jornadaFinal != null) {
							 long minutos = jornadaFinal.getTime() - parseHourDate(localT).getTime();
								minutosRestantes = (float) TimeUnit.MILLISECONDS.toMinutes(minutos);
								minutosRestantes = minutosRestantes < 0 ? 0 : minutosRestantes;
								bolsaMinutosCuadrillas = bolsaMinutosCuadrillas + minutosRestantes;
								geocercaQVO.getListCuadrillasVO().get(j).setMinutosRestantes(minutosRestantes);
						 }
						
						disponibilidadDAO.saveFAE(listFFM_AA_EVENTOS, geocercaQVO.getqInsertFAE());
					}
				}else {
					mensajes.console("processQueue", "La geocerca " + geocercaQVO.getId() +" No cuenta con cuadrillas");
				}
				
				
				minutosRestantesTurnos(listTurnos,localT);
				porcentajeTurnos(listTurnos);
				//mensajes.console("Total cuadrillas disponibles: ", ""+listCuadrillas.size());
				
				//mensajes.console("processOrder ", "Bolsa disponible de minutos: " + bolsaMinutosCuadrillas +" Geocerca: "+ geocercaQVO.getId());
				/*for (TurnoVO turno : listTurnos) {
					mensajes.console("Bolsa disponible por turno: ", "Turno: "+ turno.getFctuId()+ " : "+turno.getBolsaMinutos());
				}*/
				//Obtenemos la hora de la OT asignada mas pronto a terminar
				
				String hourOTAssign = disponibilidadDAO.getHourOTAssign(geocercaQVO.getId(),geocercaQVO.getqHourOtAssign());
				
				//Turno Matutino
				LocalDateTime localDateTime = LocalDateTime.now();
				localDateTime = localDateTime.plusHours(geocercaQVO.getHoursDifference());
				if(hourOTAssign != null && !hourOTAssign.isEmpty() && !hourOTAssign.equals("")) {
					hourOTAssign  = d[0] + "-" + d[1] + "-" + d[2] + " " + hourOTAssign;
					DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
					localDateTime = LocalDateTime.parse(hourOTAssign, format);					
				}				
				localDateTime = localDateTime.plusMinutes(0);
				LocalDateTime localDateTimeMinus = LocalDateTime.now();
				
				//Turno Vespertino
				String vespTime = d[0] + "-" + d[1] + "-" + d[2] + " "+listTurnos.get(1).getFctuHoraInicio() ;
				
				DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
				LocalDateTime localDateTimeVesp = LocalDateTime.parse(vespTime, format);
				LocalDateTime localDateTimeMinusVesp = LocalDateTime.parse(vespTime, format);
				localDateTimeVesp = localDateTimeVesp.plusMinutes(0);
				if(localDateTimeVesp.isBefore(LocalDateTime.now())) {
					localDateTimeVesp = LocalDateTime.now();
					localDateTimeMinusVesp = LocalDateTime.now();
				}				
				
				//////
				int sizeCuadrillas = listCuadrillas.size();
				float porcentaje = (15 * bolsaMinutosCuadrillas) / 100;
				DateTimeFormatter fday = DateTimeFormatter.ofPattern("HH:mm");
				float tiempoAConsumir = 0; 
				int i = 1;	
				
				//Ordenar Array de OTs Fijas
				try {					
					Collections.sort(listOTsHour, new Comparator() {
						@Override
						public int compare(Object o1, Object o2) {
							return new Integer(parseHourInt(((OTVO) o1).getFotHour())).compareTo(new Integer(parseHourInt(((OTVO) o2).getFotHour())));						
						}
					});
				} catch(Exception e) {
					mensajes.console("Ordenamiento Lista OTs", "Algo salio mal al ordenar lista de OTS: "+e.getMessage());
				}
				
				//PROCESAMIENTO DE OTS HORA FIJA POR SI EXISTEN ATRASADAS CON TOLERANCIA UNA HORA.
				
				for (OTVO OtHour : listOTsHour) {
						
					if (OtHour.getDateStart() == null) {
						//mensajes.console("0 ", "PROCESAMIENTO DE OTS HORA FIJA POR SI EXISTEN ATRASADAS CON TOLERANCIA UNA HORA");
						LocalDateTime l = LocalDateTime.now();
						if(OtHour.getFctuId() == 1) {
							l = localDateTime;
						}else if(OtHour.getFctuId() == 2) {
							l = localDateTimeVesp;
						}
						
						if (OtHour.getFotHour() != null
								&& parseHourNotDecimal(OtHour.getFotHour()) <= parseHourNotDecimal(l.format(fday))) {
							// Poner las fijas
							tiempoAConsumir = OtHour.getTimeAvg() + 30;
							String pinUpHour = d[0] + "-" + d[1] + "-" + d[2] + " " + OtHour.getFotHour();
							DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
							LocalDateTime pinUpDate = LocalDateTime.parse(pinUpHour, formatter);

							OtHour.setDateStart(pinUpDate);
							LocalDateTime localDateTimeMinusFijas = pinUpDate
									.plusMinutes((int) tiempoAConsumir);
							OtHour.setDateEnd(localDateTimeMinusFijas);

							// Dar prioridad al turno y despues a la bolsa de cuadrillas
							// By Minutes Turn
							for (TurnoVO turnoVO : listTurnos) {
								if (turnoVO.getFctuId() == OtHour.getFctuId()) {
									if (turnoVO.getBolsaMinutos() > turnoVO.getPorcentajeBolsaMinutos()) {

										// By Minutes Cuadrillas
										if (bolsaMinutosCuadrillas > porcentaje) {
											OtHour.setSemaforo(1);
										} else if (porcentaje > 0 && porcentaje < bolsaMinutosCuadrillas) {
											OtHour.setSemaforo(2);
										} else {
											OtHour.setSemaforo(3);
										}
									} else if (turnoVO.getBolsaMinutos() < turnoVO.getPorcentajeBolsaMinutos()
											&& turnoVO.getBolsaMinutos() > 0) {

										// By Minutes Cuadrillas
										if (bolsaMinutosCuadrillas > porcentaje) {
											OtHour.setSemaforo(1);
										} else if (bolsaMinutosCuadrillas < porcentaje && bolsaMinutosCuadrillas > 0) {
											OtHour.setSemaforo(2);
										} else {
											OtHour.setSemaforo(3);
										}

									} else {
										OtHour.setSemaforo(3);
									}

									if (OtHour.getFctuId() == 1) {
										listOTsResultMat.add(OtHour);
										//System.out.println(OtHour);
									}else if(OtHour.getFctuId() == 2) {
										listOTsResultVesp.add(OtHour);
										//System.out.println(OtHour);
									}
									bolsaMinutosCuadrillas = bolsaMinutosCuadrillas - tiempoAConsumir;

									if (sizeCuadrillas <= i) {
										sizeCuadrillas = sizeCuadrillas + listCuadrillas.size();
										turnoVO.setBolsaMinutos(turnoVO.getBolsaMinutos() - tiempoAConsumir);
										porcentajeTurnos(listTurnos);
									}
									
									//System.out.println("Bolsa disponible despues del calculo: "+ bolsaMinutosCuadrillas);
									/*for (TurnoVO turno : listTurnos) {
										mensajes.console("Bolsa disponible por turno despues del calculo: ", "Turno: "+ turno.getFctuId()+ " : "+turno.getBolsaMinutos());
									}*/
									i++;
								}
							}
						}
					}  
				}
				
				//PROCESAMIENTO OTS EMPRESARIALES INTERCALADAS CON OTS HORA FIJA
				for (OTVO Ot : listOTsEmp) {					
					//mensajes.console("1 ", "PROCESAMIENTO OTS MANTO RESIDENCIAL(EMPRESARIAL) INTERCALADAS CON OTS HORA FIJA");	
						
						tiempoAConsumir = Ot.getTimeAvg() + 30;

						// Dar prioridad al turno y despues a la bolsa de cuadrillas
						// By Minutes Turn
						for (TurnoVO turnoVO : listTurnos) {							
							if (turnoVO.getFctuId() == Ot.getFctuId()) {
								if (Ot.getFctuId() == 1) {
									Ot.setDateStart(localDateTime);
									localDateTimeMinus = localDateTime.plusMinutes((int) tiempoAConsumir);
									Ot.setDateEnd(localDateTimeMinus);
								}else if(Ot.getFctuId() == 2) {
									Ot.setDateStart(localDateTimeVesp);
									localDateTimeMinusVesp = localDateTimeVesp.plusMinutes((int) tiempoAConsumir);
									Ot.setDateEnd(localDateTimeMinusVesp);
								}
								if (turnoVO.getBolsaMinutos() > turnoVO.getPorcentajeBolsaMinutos()) {

									// By Minutes Cuadrillas
									if (bolsaMinutosCuadrillas > porcentaje) {
										Ot.setSemaforo(1);
									} else if (porcentaje > 0 && bolsaMinutosCuadrillas < porcentaje) {
										Ot.setSemaforo(2);
									} else {
										Ot.setSemaforo(3);
									}
								}else if (turnoVO.getBolsaMinutos() < turnoVO.getPorcentajeBolsaMinutos()
										&& turnoVO.getBolsaMinutos() > 0) {

									// By Minutes Cuadrillas
									if (bolsaMinutosCuadrillas > porcentaje) {
										Ot.setSemaforo(1);
									} else if (bolsaMinutosCuadrillas < porcentaje && bolsaMinutosCuadrillas > 0) {
										Ot.setSemaforo(2);
									} else {
										Ot.setSemaforo(3);
									}

								} else {
									Ot.setSemaforo(3);
								}
								
								if (Ot.getFctuId() == 1) {
									listOTsResultMat.add(Ot);
									//System.out.println(Ot);
								}else if(Ot.getFctuId() == 2) {
									listOTsResultVesp.add(Ot);
									//System.out.println(Ot);
								}
								
								bolsaMinutosCuadrillas = bolsaMinutosCuadrillas - tiempoAConsumir;									
								
								if(sizeCuadrillas <= i) {
									sizeCuadrillas = sizeCuadrillas + listCuadrillas.size();
									localDateTime = localDateTimeMinus;
									localDateTimeVesp = localDateTimeMinusVesp;								
									turnoVO.setBolsaMinutos(turnoVO.getBolsaMinutos() - tiempoAConsumir);
									porcentajeTurnos(listTurnos);
								}
								
								//System.out.println("Bolsa disponible despues del calculo: "+ bolsaMinutosCuadrillas);
								/*for (TurnoVO turno : listTurnos) {
									mensajes.console("Bolsa disponible por turno despues del calculo: ", "Turno: "+ turno.getFctuId()+ " : "+turno.getBolsaMinutos());
								}*/
								i++;
							} 						
						}
					
						//PROCESO OT HORA FIJA INTERCALADA CON EMPRESARIAL
						for (OTVO OtHour : listOTsHour) {
							
							if (OtHour.getDateStart() == null) {
								//mensajes.console("2 ", "PROCESO OT HORA FIJA INTERCALADA CON EMPRESARIAL");
								LocalDateTime l = LocalDateTime.now();
								if(OtHour.getFctuId() == 1) {
									l = localDateTime;
								}else if(OtHour.getFctuId() == 2) {
									l = localDateTimeVesp;
								}
								if (OtHour.getFotHour() != null
										&& parseHourNotDecimal(OtHour.getFotHour()) <= parseHourNotDecimal(l.format(fday))) {
									// Poner las fijas
									tiempoAConsumir = OtHour.getTimeAvg() + 30;
									String pinUpHour = d[0] + "-" + d[1] + "-" + d[2] + " " + OtHour.getFotHour();
									DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
									LocalDateTime pinUpDate = LocalDateTime.parse(pinUpHour, formatter);

									OtHour.setDateStart(pinUpDate);
									LocalDateTime localDateTimeMinusFijas = pinUpDate
											.plusMinutes((int) tiempoAConsumir);
									OtHour.setDateEnd(localDateTimeMinusFijas);

									// Dar prioridad al turno y despues a la bolsa de cuadrillas
									// By Minutes Turn
									for (TurnoVO turnoVO : listTurnos) {
										if (turnoVO.getFctuId() == OtHour.getFctuId()) {
											if (turnoVO.getBolsaMinutos() > turnoVO.getPorcentajeBolsaMinutos()) {

												// By Minutes Cuadrillas
												if (bolsaMinutosCuadrillas > porcentaje) {
													OtHour.setSemaforo(1);
												} else if (porcentaje > 0 && bolsaMinutosCuadrillas < porcentaje) {
													OtHour.setSemaforo(2);
												} else {
													OtHour.setSemaforo(3);
												}
											} else if (turnoVO.getBolsaMinutos() < turnoVO.getPorcentajeBolsaMinutos()
													&& turnoVO.getBolsaMinutos() > 0) {

												// By Minutes Cuadrillas
												if (bolsaMinutosCuadrillas > porcentaje) {
													OtHour.setSemaforo(1);
												} else if (bolsaMinutosCuadrillas < porcentaje && bolsaMinutosCuadrillas > 0) {
													OtHour.setSemaforo(2);
												} else {
													OtHour.setSemaforo(3);
												}

											} else {
												OtHour.setSemaforo(3);
											}

											if (OtHour.getFctuId() == 1) {
												listOTsResultMat.add(OtHour);
												//System.out.println(OtHour);
											}else if(OtHour.getFctuId() == 2) {
												listOTsResultVesp.add(OtHour);
												//System.out.println(OtHour);
											}
											bolsaMinutosCuadrillas = bolsaMinutosCuadrillas - tiempoAConsumir;

											if (sizeCuadrillas <= i) {
												sizeCuadrillas = sizeCuadrillas + listCuadrillas.size();
												turnoVO.setBolsaMinutos(turnoVO.getBolsaMinutos() - tiempoAConsumir);
												porcentajeTurnos(listTurnos);
											}
											
											//System.out.println("Bolsa disponible despues del calculo: "+ bolsaMinutosCuadrillas);
											/*for (TurnoVO turno : listTurnos) {
												mensajes.console("Bolsa disponible por turno despues del calculo: ", "Turno: "+ turno.getFctuId()+ " : "+turno.getBolsaMinutos());
											}*/
											i++;
										}
									}
								}
							} 
						}															
				}	
				
				//PROCESAMIENTO OTS RESIDENCIALES INTERCALADAS CON OTS HORA FIJA
				for (OTVO oTSop : listOTsSop) {				
					//Resto de OTs
					//mensajes.console("3 ", "PROCESAMIENTO OTS RESIDENCIALES INTERCALADAS CON OTS HORA FIJA");
					tiempoAConsumir = oTSop.getTimeAvg() + 30;

					// Dar prioridad al turno y despues a la bolsa de cuadrillas
					// By Minutes Turn
					for (TurnoVO turnoVO : listTurnos) {
						
						if (turnoVO.getFctuId() == oTSop.getFctuId()) {
							if (oTSop.getFctuId() == 1) {
								oTSop.setDateStart(localDateTime);
								localDateTimeMinus = localDateTime.plusMinutes((int) tiempoAConsumir);
								oTSop.setDateEnd(localDateTimeMinus);
							}else if(oTSop.getFctuId() == 2) {
								oTSop.setDateStart(localDateTimeVesp);
								localDateTimeMinusVesp = localDateTimeVesp.plusMinutes((int) tiempoAConsumir);
								oTSop.setDateEnd(localDateTimeMinusVesp);
							}
							if (turnoVO.getBolsaMinutos() > turnoVO.getPorcentajeBolsaMinutos()) {

								// By Minutes Cuadrillas
								if (bolsaMinutosCuadrillas > porcentaje) {
									oTSop.setSemaforo(1);
								} else if (porcentaje > 0 && bolsaMinutosCuadrillas < porcentaje) {
									oTSop.setSemaforo(2);
								} else {
									oTSop.setSemaforo(3);
								}
							}else if (turnoVO.getBolsaMinutos() < turnoVO.getPorcentajeBolsaMinutos()
									&& turnoVO.getBolsaMinutos() > 0) {

								// By Minutes Cuadrillas
								if (bolsaMinutosCuadrillas > porcentaje) {
									oTSop.setSemaforo(1);
								} else if (bolsaMinutosCuadrillas < porcentaje  && bolsaMinutosCuadrillas > 0) {
									oTSop.setSemaforo(2);
								} else {
									oTSop.setSemaforo(3);
								}

							} else {
								oTSop.setSemaforo(3);
							}							
							
							if (oTSop.getFctuId() == 1) {
								listOTsResultMat.add(oTSop);
								//System.out.println(oTSop);
							}else if(oTSop.getFctuId() == 2) {
								listOTsResultVesp.add(oTSop);
								//System.out.println(oTSop);
							}
							bolsaMinutosCuadrillas = bolsaMinutosCuadrillas - tiempoAConsumir;
							
							if(sizeCuadrillas <= i) {
								sizeCuadrillas = sizeCuadrillas + listCuadrillas.size();
								localDateTime = localDateTimeMinus;
								localDateTimeVesp = localDateTimeMinusVesp;				
								turnoVO.setBolsaMinutos(turnoVO.getBolsaMinutos() - tiempoAConsumir);
								porcentajeTurnos(listTurnos);
							}
							
							/*System.out.println("Bolsa disponible despues del calculo: "+ bolsaMinutosCuadrillas);
							for (TurnoVO turno : listTurnos) {
								mensajes.console("Bolsa disponible por turno despues del calculo: ", "Turno: "+ turno.getFctuId()+ " : "+turno.getBolsaMinutos());
							}*/
							i++;
						} 
					}				
					
					//PROCESO OT HORA FIJA INTERCALADA CON RESIDENCIAL
						for (OTVO OtHour : listOTsHour) {
							
							if (OtHour.getDateStart() == null) {
								mensajes.console("4 ", "PROCESO OT HORA FIJA INTERCALADA CON RESIDENCIAL");
								LocalDateTime l = LocalDateTime.now();
								if(OtHour.getFctuId() == 1) {
									l = localDateTime;
								}else if(OtHour.getFctuId() == 2) {
									l = localDateTimeVesp;
								}
								
								if (OtHour.getFotHour() != null
										&& parseHourNotDecimal(OtHour.getFotHour()) <= parseHourNotDecimal(l.format(fday))) {
									// Poner las fijas
									tiempoAConsumir = OtHour.getTimeAvg() + 30;
									String pinUpHour = d[0] + "-" + d[1] + "-" + d[2] + " " + OtHour.getFotHour();
									DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
									LocalDateTime pinUpDate = LocalDateTime.parse(pinUpHour, formatter);

									OtHour.setDateStart(pinUpDate);
									LocalDateTime localDateTimeMinusFijas = pinUpDate
											.plusMinutes((int) tiempoAConsumir);
									OtHour.setDateEnd(localDateTimeMinusFijas);

									// Dar prioridad al turno y despues a la bolsa de cuadrillas
									// By Minutes Turn
									for (TurnoVO turnoVO : listTurnos) {
										if (turnoVO.getFctuId() == OtHour.getFctuId()) {
											if (turnoVO.getBolsaMinutos() > turnoVO.getPorcentajeBolsaMinutos()) {

												// By Minutes Cuadrillas
												if (bolsaMinutosCuadrillas > porcentaje) {
													OtHour.setSemaforo(1);
												} else if (porcentaje > 0 && bolsaMinutosCuadrillas < porcentaje) {
													OtHour.setSemaforo(2);
												} else {
													OtHour.setSemaforo(3);
												}
											} else if (turnoVO.getBolsaMinutos() < turnoVO.getPorcentajeBolsaMinutos()
													&& turnoVO.getBolsaMinutos() > 0) {

												// By Minutes Cuadrillas
												if (bolsaMinutosCuadrillas > porcentaje) {
													OtHour.setSemaforo(1);
												} else if (bolsaMinutosCuadrillas < porcentaje && bolsaMinutosCuadrillas > 0) {
													OtHour.setSemaforo(2);
												} else {
													OtHour.setSemaforo(3);
												}

											} else {
												OtHour.setSemaforo(3);
											}

											if (OtHour.getFctuId() == 1) {
												listOTsResultMat.add(OtHour);
												//System.out.println(OtHour);
											}else if(OtHour.getFctuId() == 2) {
												listOTsResultVesp.add(OtHour);
												//System.out.println(OtHour);
											}
											bolsaMinutosCuadrillas = bolsaMinutosCuadrillas - tiempoAConsumir;

											if (sizeCuadrillas <= i) {
												sizeCuadrillas = sizeCuadrillas + listCuadrillas.size();
												turnoVO.setBolsaMinutos(turnoVO.getBolsaMinutos() - tiempoAConsumir);
												porcentajeTurnos(listTurnos);
											}
											
											//System.out.println("Bolsa disponible despues del calculo: "+ bolsaMinutosCuadrillas);
											/*for (TurnoVO turno : listTurnos) {
												mensajes.console("Bolsa disponible por turno despues del calculo: ", "Turno: "+ turno.getFctuId()+ " : "+turno.getBolsaMinutos());
											}*/
											i++;
										}
									}
								}
							}  
						}			
				}
								
				//PROCESAMIENTO DE OTS HORA FIJA POR SI QUEDA ALGUNA PENDIENTE
				
				for (OTVO OtHour : listOTsHour) {
					
					if (OtHour.getDateStart() == null) {
						//mensajes.console("5 ", "PROCESAMIENTO DE OTS HORA FIJA POR SI QUEDA ALGUNA PENDIENTE");
//						LocalDateTime l = LocalDateTime.now();
//						if(OtHour.getFctuId() == 1) {
//							l = localDateTime;
//						}else if(OtHour.getFctuId() == 2) {
//							l = localDateTimeVesp;
//						}
						
							// Poner las fijas
							tiempoAConsumir = OtHour.getTimeAvg() + 30;
							String pinUpHour = d[0] + "-" + d[1] + "-" + d[2] + " " + OtHour.getFotHour();
							DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
							LocalDateTime pinUpDate = LocalDateTime.parse(pinUpHour, formatter);

							OtHour.setDateStart(pinUpDate);
							LocalDateTime localDateTimeMinusFijas = pinUpDate
									.plusMinutes((int) tiempoAConsumir);
							OtHour.setDateEnd(localDateTimeMinusFijas);

							// Dar prioridad al turno y despues a la bolsa de cuadrillas
							// By Minutes Turn
							for (TurnoVO turnoVO : listTurnos) {
								if (turnoVO.getFctuId() == OtHour.getFctuId()) {
									if (turnoVO.getBolsaMinutos() > turnoVO.getPorcentajeBolsaMinutos()) {

										// By Minutes Cuadrillas
										if (bolsaMinutosCuadrillas > porcentaje) {
											OtHour.setSemaforo(1);
										} else if (porcentaje > 0 && bolsaMinutosCuadrillas < porcentaje) {
											OtHour.setSemaforo(2);
										} else {
											OtHour.setSemaforo(3);
										}
									} else if (turnoVO.getBolsaMinutos() < turnoVO.getPorcentajeBolsaMinutos()
											&& turnoVO.getBolsaMinutos() > 0) {

										// By Minutes Cuadrillas
										if (bolsaMinutosCuadrillas > porcentaje) {
											OtHour.setSemaforo(1);
										} else if (bolsaMinutosCuadrillas < porcentaje && bolsaMinutosCuadrillas > 0) {
											OtHour.setSemaforo(2);
										} else {
											OtHour.setSemaforo(3);
										}

									} else {
										OtHour.setSemaforo(3);
									}

									if (OtHour.getFctuId() == 1) {
										listOTsResultMat.add(OtHour);
										//System.out.println(OtHour);
									}else if(OtHour.getFctuId() == 2) {
										listOTsResultVesp.add(OtHour);
										//System.out.println(OtHour);
									}
									bolsaMinutosCuadrillas = bolsaMinutosCuadrillas - tiempoAConsumir;

									if (sizeCuadrillas <= i) {
										sizeCuadrillas = sizeCuadrillas + listCuadrillas.size();
										turnoVO.setBolsaMinutos(turnoVO.getBolsaMinutos() - tiempoAConsumir);
										porcentajeTurnos(listTurnos);
									}
									
									//System.out.println("Bolsa disponible despues del calculo: "+ bolsaMinutosCuadrillas);
									/*for (TurnoVO turno : listTurnos) {
										mensajes.console("Bolsa disponible por turno despues del calculo: ", "Turno: "+ turno.getFctuId()+ " : "+turno.getBolsaMinutos());
									}*/
									i++;
								}
							}
					}  
				}
				//Se elimina queue de geocerca
				mensajes.console("6", "Se inicia eliminar registros de queue_theory para geocerca: "+geocercaQVO.getId());	
				boolean a = disponibilidadDAO.deleteGeocerca(geocercaQVO);
				
				//Guarda en ffm_queue_theory
				if(a) {
					
					mensajes.console("7", "Se eliminaron registros con exito");
					if(listOTsResultMat != null && !listOTsResultMat.isEmpty()) {
						List<FFM_QUEUE_THEORY> listFFM_QUEUE_THEORY = new ArrayList<FFM_QUEUE_THEORY>();
					//	mensajes.console("8", "El arreglo listOTsResultMat viene lleno");
						for (OTVO otMat : listOTsResultMat) {	
							FFM_QUEUE_THEORY fqt = new FFM_QUEUE_THEORY();
							fqt.setFOT_ID(otMat.getFotId());
							fqt.setTIPO(otMat.getFciDescription());
							fqt.setFSO_OS(otMat.getFsoOs());
							fqt.setHORA_INICIO(otMat.getDateStart());
							fqt.setHORA_FIN(otMat.getDateEnd());
							fqt.setSEMAFORO(otMat.getSemaforo());
							fqt.setGEOCERCA(otMat.getZone());
							fqt.setFOT_CONFIRMATION(otMat.getFotConfirmation());
							fqt.setFCT_ID(otMat.getIdGeo());
							fqt.setECI_ID(otMat.getFciIdSubTypeInter());
							fqt.setECT_ID(otMat.getEctId());
							fqt.setTURNO(otMat.getFctuId());
							//mensajes.console("8", "AGREGADOS A LA LISTA: "+fqt);
							listFFM_QUEUE_THEORY.add(fqt);
						}
						if(disponibilidadDAO.saveAllOTQ(listFFM_QUEUE_THEORY,geocercaQVO.getqInsertFfmQueue())) {
							mensajes.console("9", "INSERTADOS CON EXITO: ");
						}
					}
					
					if(listOTsResultVesp != null && !listOTsResultVesp.isEmpty()) {
						List<FFM_QUEUE_THEORY> listFFM_QUEUE_THEORY = new ArrayList<FFM_QUEUE_THEORY>();
					//	mensajes.console("10", "El arreglo listOTsResultVesp viene lleno");
						for (OTVO otVesp : listOTsResultVesp) {		
							FFM_QUEUE_THEORY fct = new FFM_QUEUE_THEORY();
							fct.setFOT_ID(otVesp.getFotId());
							fct.setTIPO(otVesp.getFciDescription());
							fct.setFSO_OS(otVesp.getFsoOs());
							fct.setHORA_INICIO(otVesp.getDateStart());
							fct.setHORA_FIN(otVesp.getDateEnd());
							fct.setSEMAFORO(otVesp.getSemaforo());
							fct.setGEOCERCA(otVesp.getZone());
							fct.setFOT_CONFIRMATION(otVesp.getFotConfirmation());
							fct.setFCT_ID(otVesp.getIdGeo());
							fct.setECI_ID(otVesp.getFciIdSubTypeInter());
							fct.setECT_ID(otVesp.getEctId());
							fct.setTURNO(otVesp.getFctuId());
							listFFM_QUEUE_THEORY.add(fct);
							
						}
						if(disponibilidadDAO.saveAllOTQ(listFFM_QUEUE_THEORY,geocercaQVO.getqInsertFfmQueue())) {
							mensajes.console("9", "INSERTADO CON EXITO: ");
						}
					}
				}							
			}
			
		}catch(Exception e) {
			mensajes.console("process", "Algo salio mal exception: " + e.getMessage());
			for (Object o : e.getStackTrace()) {
				mensajes.console("process", "Algo salio mal exception: " + o.toString());		
			}					
		}
	}
	
	private int exeLockUnlock(List<ParametrosVO> listParametrosVO, List<GeocercaVO> listGeocercaVO, ArgumentosVO argumentosVO) {
		int resetParameters = 0;
		try {			
			String[]  queryLockUnlock= {};
			String queryValidateLock = "";	
			String qGetSemaforo = "";			
			float startHour = 0;
			float endHour = 0;
			float now = 0;
			if (listParametrosVO.size() > 0) {										
				for (ParametrosVO parametrosVO : listParametrosVO) {
					switch(parametrosVO.getFapId()-argumentosVO.getConfiguracion()) {
						case Constantes.RANGOEJECUCION:
								String[] schedule = parametrosVO.getFapValor().split(",");
								startHour = parseHour(schedule[1]);
								endHour = parseHour(schedule[2]);
								now = parseHour("" + LocalTime.now());						
							break;
						case Constantes.VALIDALOCK:
								queryValidateLock = parametrosVO.getFapValor();
							break;
						case Constantes.LOCKUNLOCK:
								queryLockUnlock = parametrosVO.getFapValor().split("-");
							break;
						case Constantes.QGETSEMAFORO:
							String[] p = parametrosVO.getFapValor().split("\\|");
							qGetSemaforo = p[1];
							break;					
					}
				}
			} else {
				mensajes.console("Ejecuta Disponibilidad", "Algo salio mal los parametros vienen null");
			}
			
			if (now > startHour && now < endHour) {
				//Entra al ciclo validando horario de geocerca	
				for (GeocercaVO geocercaVO : listGeocercaVO) {
					//Valida horario local de geocerca
					String localTime =""+LocalTime.now().plusHours(geocercaVO.getHoursDifference()); 
					if(parseHour(localTime) > parseHour(geocercaVO.getStartTime()) && parseHour(localTime) < parseHour(geocercaVO.getEndTime())) {
						Integer semaforo = disponibilidadDAO.exeLockUnlock(geocercaVO,qGetSemaforo);	
							mensajes.console("Ejecuta Disponibilidad",
									semaforo != null ? "Se ejecuta consulta de semaforos" : "Algo salio mal al consultar semaforos");
						if (geocercaVO.isEnabled()) {							
							//semaforo = 0;
							if (semaforo != null) {
							    boolean existDisp = false;
								mensajes.console("Ejecuta Disponibilidad",
										"Semaforo "+semaforo+" para la geocerca "
												+ geocercaVO.getId());
								if(validaS(queryValidateLock)) {
									existDisp = lockUnlock(semaforo, geocercaVO,queryValidateLock,queryLockUnlock);
								}else {
									mensajes.console("validaS", "El query no paso la prueba de validacion, se aborta proceso");
									return -1;
								}	
								if(existDisp) {
									mensajes.console("Ejecuta Disponibilidad", semaforo == 1 ? "La disponibilidad se BLOQUEO con exito"
											: "La disponibilidad se DESBLOQUEO con exito");	
								}
								
							} else {
								mensajes.console("Ejecuta Disponibilidad",
										"Algo salio mal al consultar semaforo, viene null");
							}							
						} else {
							mensajes.console("Ejecuta Disponibilidad","La bandera de bloquedo de disponibilidad no se encuentra habilitada para esta geocerca: "+geocercaVO.getId());
						}
						
					} else {
						mensajes.console("Ejecuta Disponibilidad", "La geocerca: "+ geocercaVO.getId() +" se encuentra fuera de horario.");
					}				
				}
				now = parseHour("" + LocalTime.now());
			}		

		} catch (Exception e) {
			mensajes.console("Ejecuta Disponibilidad", "Algo salio mal exception: " + e.getMessage());
		}
		return resetParameters;
	}
	
	private boolean lockUnlock(Integer semaforo, GeocercaVO geocerca, String queryValidateLock, String[] queryLockUnlock) {
		try {
			int[] validateLock = disponibilidadDAO.validateLock(geocerca,queryValidateLock);
			if (validateLock != null) {
				if(semaforo == 1 && validateLock[0] > validateLock[1]) {
					boolean flag = disponibilidadDAO.lockUnlock(semaforo, geocerca,validateLock,queryLockUnlock);
					mensajes.console("Disponibilidad",
							flag ? "Se actualizo disponibilidad para la geocerca: " + geocerca.getId()
									: "Algo salio mal al actualizar disponibilidad");
					return flag;
				}else if(semaforo == 1 && validateLock[1] > validateLock[0]){
					mensajes.console("Disponibilidad", "La geocerca ya esta bloqueada: "+ geocerca.getId());
				}else if(semaforo == 0 && validateLock[1] > validateLock[0]) {
					boolean flag = disponibilidadDAO.lockUnlock(semaforo, geocerca,validateLock,queryLockUnlock);
					mensajes.console("Disponibilidad",
							flag ? "Se actualizo disponibilidad para la geocerca: " + geocerca.getId()
									: "Algo salio mal al actualizar disponibilidad");
					return flag;
				}else if(semaforo == 0 && validateLock[0] > validateLock[1]){
					mensajes.console("Disponibilidad", "La geocerca ya esta desbloqueada: "+ geocerca.getId());
				}else if(validateLock[0] == validateLock[1]) {
					if(validateLock[0] != 0 && validateLock[1] != 0) {
						boolean flag = disponibilidadDAO.lockUnlock(semaforo, geocerca,validateLock,queryLockUnlock);
						mensajes.console("Disponibilidad",
								flag ? "Se actualizo disponibilidad para la geocerca: " + geocerca.getId()
										: "Algo salio mal al actualizar disponibilidad");
						return flag;
					}else {
						mensajes.console("Disponibilidad", "la geocerca no cuenta con valores de disponibilidad");
					}					
				}
			} else {
				mensajes.console("Ejecuta Disponibilidad", "Algo salio mal al obtener disponibilidad de geocerca");
				return false;
			}
		} catch (Exception e) {
			mensajes.console("Disponibilidad", "Algo salio mal exception: " + e.getMessage());
			return false;
		}
		return false;
	}
	
	private float parseHour(String date) {
		String[] parse = date.split(":");
		String format = parse[0]+"."+parse[1];
		return Float.parseFloat(format);
	}
	
	private boolean validaS(String q) {
		String[] s = q.split(" ");
		for (String token : s) {
			return token.toUpperCase().equals("DROP") ? false
					: token.toUpperCase().equals("TRUNCATE") ? false
							: token.toUpperCase().equals("DELETE") ? false
									: token.toUpperCase().equals("UPDATE") ? false : true;

		}
		return false;
	}
	
	private float setTiempoPromedio(OTVO otvo, String tiempoPromedio) {
		tiempoPromedio = tiempoPromedio.substring(26,tiempoPromedio.length()-1);		
		
			String[] v = tiempoPromedio.split("\\),\\(");
			for (int i = 0; i < v.length; i++) {
				String[] j = v[i].split(",");
				try {
					if(otvo.getFciIdSubTypeInter() == Integer.parseInt(j[0])) {
						return Float.parseFloat(j[1]);
					}
				} catch(NumberFormatException nfe) {
					mensajes.console("setTiempoPromedio", "Algo salio mal exception: " + nfe.getMessage());
				}				
			}		
		return 0;
	}
	
	private void porcentajeTurnos(List<TurnoVO> listTurnos) {
		for (TurnoVO turnoVO : listTurnos) {
			turnoVO.setPorcentajeBolsaMinutos((15 * turnoVO.getBolsaMinutos()) / 100);
		}		
	}

	private void minutosRestantesTurnos(List<TurnoVO> listTurnos, String localT) {
		float bolsaMinutosTurnos = 0;
		for (TurnoVO turnoVO : listTurnos) {
			long diferencia = parseHourDate(turnoVO.getFctuHoraFin()).getTime() - parseHourDate(localT).getTime();
			long min = TimeUnit.MILLISECONDS.toMinutes(diferencia);		
			bolsaMinutosTurnos = (float) min;
			turnoVO.setBolsaMinutos(bolsaMinutosTurnos);			
		}		
	}
	
	private Date parseHourDate(String date) {
		SimpleDateFormat formato = new SimpleDateFormat("HH:mm");
        Date fechaDate = null;
        try {
            fechaDate = formato.parse(date);
        } 
        catch (ParseException ex) 
        {
            System.out.println(ex);
        }
        return fechaDate;
	}
	
	private Integer parseHourInt(String date) {
		String[] d = date.split(":");
        
        return Integer.parseInt(d[0]+d[1]);
	}
	
	private Integer parseHourNotDecimal(String date) {
		String[] d = date.split(":");        
        return Integer.parseInt(d[0]);
	}
}