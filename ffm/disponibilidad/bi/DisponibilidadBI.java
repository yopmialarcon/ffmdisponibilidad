package ffm.disponibilidad.bi;

import java.util.List;

import ffm.disponibilidad.vo.ArgumentosVO;
import ffm.disponibilidad.vo.GeocercaQVO;
import ffm.disponibilidad.vo.GeocercaVO;
import ffm.disponibilidad.vo.ParametrosVO;
import ffm.disponibilidad.vo.TurnoVO;

public interface DisponibilidadBI {
	public ArgumentosVO setArguments(String[] args);
	public List<ParametrosVO>  getParameters(ArgumentosVO argumentsVO);
	public List<GeocercaVO> getGeocercas(List<ParametrosVO> parameters);
	public int process(List<GeocercaQVO> listGeocercasQVO, ArgumentosVO argumentsVO,List<ParametrosVO> parameters, List<GeocercaVO> geocercas, List<TurnoVO> listTurnos);
	public List<GeocercaQVO> getGeocercasQ(List<ParametrosVO> parameters);
	public List<TurnoVO> getTurns(List<ParametrosVO> parameters);
}
