package ffm.disponibilidad.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSourceUtils;
import org.springframework.stereotype.Component;

import ffm.disponibilidad.entidades.FFM_AA_EVENTOS;
import ffm.disponibilidad.entidades.FFM_QUEUE_THEORY;
import ffm.disponibilidad.util.Constantes;
import ffm.disponibilidad.util.IMensajes;
import ffm.disponibilidad.vo.ArgumentosVO;
import ffm.disponibilidad.vo.CuadrillasVO;
import ffm.disponibilidad.vo.GeocercaQVO;
import ffm.disponibilidad.vo.GeocercaVO;
import ffm.disponibilidad.vo.JornadaVO;
import ffm.disponibilidad.vo.OTVO;
import ffm.disponibilidad.vo.ParametrosVO;
import ffm.disponibilidad.vo.TurnoVO;

@Component
public class DisponibilidadDAOImpl implements DisponibilidadDAO{
	
	private JdbcTemplate jdbcTemplateA;
	private JdbcTemplate jdbcTemplateB;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplateA;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplateB;
		
	@Autowired
	private IMensajes mensajes;

	@Autowired
	private void setDataSource(javax.sql.DataSource dataSourceA, javax.sql.DataSource dataSourceB) {
			
		this.jdbcTemplateA = new JdbcTemplate(dataSourceA);
		this.jdbcTemplateB = new JdbcTemplate(dataSourceB);
		this.namedParameterJdbcTemplateA = new NamedParameterJdbcTemplate(dataSourceA);
		this.namedParameterJdbcTemplateB = new NamedParameterJdbcTemplate(dataSourceB);
	}	
	
	@Override
	public List<ParametrosVO> getParameters(ArgumentosVO argumentsVO) {
		try {
			Map<String, Object> parametres = new HashMap<String, Object>();
			parametres.put("FAP_STATUS", Constantes.FAPSTATUS);
			parametres.put("FAP_ID", Constantes.FAPID_GETPARAMETERS+argumentsVO.getConfiguracion());
			parametres.put("FAP_IDR", Constantes.FAPID_GETPARAMETERS+argumentsVO.getConfiguracion()+999);
			parametres.put("FAP_IDQ", Constantes.FAPID_GETPARAMETERS+argumentsVO.getConfiguracion()+61000);
			parametres.put("FAP_IDRQ", Constantes.FAPID_GETPARAMETERS+argumentsVO.getConfiguracion()+63999);
			parametres.put("FAP_IDC", 402);
			return namedParameterJdbcTemplateA.query("SELECT FAP.FAP_ID, FAP.FAP_STATUS,FAP.FAP_VALOR FROM FFM_AA_PARAMETROS FAP WHERE FAP.FAP_STATUS = :FAP_STATUS AND FAP.FAP_ID BETWEEN :FAP_ID AND :FAP_IDR OR FAP.FAP_ID BETWEEN :FAP_IDQ AND :FAP_IDRQ OR FAP.FAP_ID = :FAP_IDC", 
					new MapSqlParameterSource(parametres), new RowMapper<ParametrosVO>() {
				
				@Override
				public ParametrosVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					ParametrosVO parametersVO = new ParametrosVO();
					parametersVO.setFapId(rs.getInt("FAP_ID"));
					parametersVO.setFapStatus(rs.getLong("FAP_STATUS"));
					parametersVO.setFapValor(rs.getString("FAP_VALOR"));
					return parametersVO;
				}
				
			});
		}catch(Exception e) {
			mensajes.console("getParameters", "Algo salio mal al obtener los parametros: " + e.getMessage());
			return null;
		}	
	}

	@Override
	public void getTimeZone(String query, GeocercaVO geocercaVO, GeocercaQVO geocercaQVO) {		
		
		int fctId =  geocercaVO != null ? geocercaVO.getId() : geocercaQVO.getId();
		try {
			namedParameterJdbcTemplateA.queryForObject(query, new MapSqlParameterSource("FCT_ID", fctId),
					new RowMapper<GeocercaVO>() {
						@Override
						public GeocercaVO mapRow(ResultSet rs, int arg1) throws SQLException {
							if(geocercaVO != null) {
								geocercaVO.setTimeZone(rs.getString("FCT_TIME_ZONE"));
								geocercaVO.setHoursDifference(rs.getInt("HORASDEDIFERENCIA"));
							} else {
								geocercaQVO.setTimeZone(rs.getString("FCT_TIME_ZONE"));
								geocercaQVO.setHoursDifference(rs.getInt("HORASDEDIFERENCIA"));
							}
							
							return geocercaVO;
						}
					});
		}catch(Exception e) {
			mensajes.console("getTimeZone", "Algo salio mal al obtener getTimeZone para la geocerca , exception: " + e.getMessage());
		}
	}

	@Override
	public Integer exeLockUnlock(GeocercaVO geocercaVO, String qGetSemaforo) {
		try {
			
			return namedParameterJdbcTemplateA.queryForObject(qGetSemaforo, new MapSqlParameterSource("FCT_ID", geocercaVO.getId()),
					new RowMapper<Integer>() {
						@Override
						public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
							if(!rs.getString(1).isEmpty() && rs.getString(1) != null & !rs.getString(1).equals("")) {
								return rs.getInt(1) == 3 ? 1 : 0;
							}else {
								mensajes.console("exeLockUnlock", "No hay semaforo en rojo para la geocerca: "+ geocercaVO.getId());
								return -1;
							}							
						}
					});
		}catch(Exception e) {
			
			if(e.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
				mensajes.console("exeLockUnlock", "No hay semaforo en rojo");
				return 0;
			}else {
				mensajes.console("exeLockUnlock", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			}
			return -1;
		}
	}

	@Override
	public int getResetParameters(ArgumentosVO argumentsVO) {
		try {
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("FAP_STATUS", Constantes.FAPSTATUS);
			parametros.put("FAP_ID", argumentsVO.getConfiguracion() + Constantes.FAPID_RESETPARAMETERS);
			
			return namedParameterJdbcTemplateA.queryForObject("SELECT FAP.FAP_VALOR FROM FFM_AA_PARAMETROS FAP WHERE FAP.FAP_ID = :FAP_ID AND FAP.FAP_STATUS = :FAP_STATUS", new MapSqlParameterSource(parametros),
					new RowMapper<Integer>() {
						@Override
						public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
							if(!rs.getString(1).isEmpty() && rs.getString(1) != null & !rs.getString(1).equals("")) {
								String[] r = rs.getString(1).split(",");
								return r[1].equals("") || r[1].isEmpty() ? -1 : Integer.parseInt(r[1]);
							}else {
								mensajes.console("getResetParameters", "Algo salio mal, no se pudo obtener el parametro para reset de parametros su valor es: "+ rs.getString(1));
								return -1;
							}							
						}
					});
		}catch(Exception e) {
			mensajes.console("getResetParameters", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			return -1;
		}		
	}

	@Override
	public int[] validateLock(GeocercaVO geocercaVO, String queryValidateLock) {
		try {
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("FCI_APLICA_DISPO", 1);
			parametros.put("FCI_ID_PARENT", 48);
			parametros.put("FCI_ID_PARENT2", 55);
			parametros.put("FCI_ID_PARENT3", 65);
			parametros.put("FDGE_COMPANIA", 1);
			parametros.put("FCT_GEOCERCA", geocercaVO.getId());
			int[] validateLock = new int[2];
			return namedParameterJdbcTemplateA.queryForObject(queryValidateLock, new MapSqlParameterSource(parametros),
					new RowMapper<int[]>() {
						@Override
						public int[] mapRow(ResultSet rs, int arg1) throws SQLException {
							if(rs.getInt("FDGE_BLOQUEO") == 0) {
								validateLock[0] = rs.getInt("COUNT");
							}
							else if(rs.getInt("FDGE_BLOQUEO") == 1) {
								validateLock[1] = rs.getInt("COUNT");
							}
							return validateLock;
						}
					});
			
		}catch(Exception e) {
			if(e.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
				mensajes.console("validateLock", "002 Falla en los datos. NO HAY DISPONIBILIDAD PARA LA GEOCERCA");
			}else {
				mensajes.console("validateLock", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			}			
			return null;
		}
	}

	@Override
	public boolean lockUnlock(Integer semaforo, GeocercaVO geocercaVO, int[] validateLock, String[] queryLockUnlock) {
		try {			
			String validate = semaforo == 1 && validateLock[0] > validateLock[1]
					? "CASE WHEN FDGE_BLOQUEO = 0 THEN 1 WHEN FDGE_BLOQUEO = 1 THEN -1 ELSE 1 END"
					: semaforo == 0 && validateLock[1] > validateLock[0] ? "CASE WHEN FDGE_BLOQUEO = 1 THEN 0 WHEN FDGE_BLOQUEO = -1 THEN 1 ELSE 0 END" 
							: semaforo == 1 && validateLock[1] == validateLock[0] ? "CASE WHEN FDGE_BLOQUEO = 0 THEN 1 WHEN FDGE_BLOQUEO = 1 THEN -1 ELSE 1 END" 
									: semaforo == 0 && validateLock[1] == validateLock[0] ? "CASE WHEN FDGE_BLOQUEO = 1 THEN 0 WHEN FDGE_BLOQUEO = -1 THEN 1 ELSE 0 END" : "FDGE_BLOQUEO";
			String query  = queryLockUnlock[0]+ validate +queryLockUnlock[1];	
			
			return namedParameterJdbcTemplateA.update(query, 
					new MapSqlParameterSource("FCT_GEOCERCA", geocercaVO.getId())) == 1;
			
		}catch(Exception e) {
			mensajes.console("lockUnlock", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			return false;
		}
	}

	@Override
	public void updateResetParameters(String queryUpdateResetParams,ArgumentosVO argumentosVO) {
		try {
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("FAP_VALOR", "ResetPamaramsFFMDisponibilidad,0");
			parametros.put("FAP_ID", 20010);
			namedParameterJdbcTemplateA.update(queryUpdateResetParams,	new MapSqlParameterSource(parametros));
		}catch(Exception e) {
			mensajes.console("updateResetParameters", "002 Falla en los datos. Algo salio mal al resetear parametros, exception: "+ e.getMessage());
		}		
	}

	/*@Override
	public List<OTVO> getListOTsEmp(GeocercaQVO geocercaQVO) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FCT_ID_PARENT", geocercaQVO.getId());
			parameters.put("FCI_ID_SUB_TYPE_INTER", geocercaQVO.getSkills());
			return namedParameterJdbcTemplateA.query(geocercaQVO.getqOTs(), new MapSqlParameterSource(parameters), new RowMapper<OTVO>() {
				
				@Override
				public OTVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					OTVO otVO = new OTVO();
					otVO.setVip(rs.getInt(1));
					otVO.setFotTicketsRaised(rs.getInt(2));
					otVO.setTurnoInicia(rs.getString(3));
					otVO.setFotId(rs.getInt(4));
					otVO.setFciIdSubTypeInter(rs.getInt(5));
					otVO.setFctId(rs.getInt(6));
					otVO.setFotConfirmation(rs.getInt(7));
					otVO.setFsoOs(rs.getString(8));
					otVO.setFciDescription(rs.getString(9));
					otVO.setFctuId(rs.getInt(10));
					otVO.setZone(rs.getString(11));
					otVO.setEctId(rs.getInt(12));
					otVO.setFotHour(rs.getString(13));
					otVO.setIdGeo(geocercaQVO.getId());
					return otVO;
				}				
			});
		}catch(Exception e) {
			mensajes.console("getListOTs", "Algo salio mal al obtener las OTs: " + e.getMessage());
			return null;
		}		
	}*/
	
	@Override
	public List<OTVO> getListOTs(GeocercaQVO geocercaQVO) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FCT_ID_PARENT", geocercaQVO.getId());
			parameters.put("FCI_ID_SUB_TYPE_INTER", geocercaQVO.getSkills());
			return namedParameterJdbcTemplateA.query(geocercaQVO.getqOTs(), new MapSqlParameterSource(parameters), new RowMapper<OTVO>() {
				
				@Override
				public OTVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					OTVO otVO = new OTVO();
					otVO.setVip(rs.getInt(1));
					otVO.setFotTicketsRaised(rs.getInt(2));
					otVO.setTurnoInicia(rs.getString(3));
					otVO.setFotId(rs.getInt(4));
					otVO.setFciIdSubTypeInter(rs.getInt(5));
					otVO.setFctId(rs.getInt(6));
					otVO.setFotConfirmation(rs.getInt(7));
					otVO.setFsoOs(rs.getString(8));
					otVO.setFciDescription(rs.getString(9));
					otVO.setFctuId(rs.getInt(10));
					otVO.setZone(rs.getString(11));
					otVO.setEctId(rs.getInt(12));
					otVO.setFotHour(rs.getString(13));
					otVO.setIdGeo(geocercaQVO.getId());
					return otVO;
				}				
			});
		}catch(Exception e) {
			mensajes.console("getListOTs", "Algo salio mal al obtener las OTs: " + e.getMessage());
			return new ArrayList<OTVO>();
		}		
	}

	@Override
	public List<CuadrillasVO> getListCuadrillas(GeocercaQVO geocercaQVO) {
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("FCW_FCT_ID", geocercaQVO.getId());
			parameters.put("FCI_ID", geocercaQVO.getSkills());
			return namedParameterJdbcTemplateA.query(geocercaQVO.getqCuadrillas(), new MapSqlParameterSource(parameters), new RowMapper<CuadrillasVO>() {
				
				@Override
				public CuadrillasVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					CuadrillasVO cuadrillasVO = new CuadrillasVO();
					cuadrillasVO.setFfoId(rs.getInt(1));
					cuadrillasVO.setLat(rs.getDouble(2));
					cuadrillasVO.setLen(rs.getDouble(3));
					cuadrillasVO.setFcopeId(rs.getInt(4));
					cuadrillasVO.setFctId(rs.getInt(5));
					cuadrillasVO.setFctIds(rs.getString(6));
					cuadrillasVO.setJornadaVO(getJornada(geocercaQVO.getqJornada(),rs.getInt(1)));
					//System.out.println("OT: "+rs.getInt(1) + "LAT: " +rs.getDouble(2)+ "LEN: " +rs.getDouble(3)+"FCOPEID: "+rs.getInt(4)+"FCT_ID: "+rs.getInt(5)+"FCI_IDS "+rs.getString(6));
					return cuadrillasVO;
				}				
			});
		}catch(Exception e) {
			mensajes.console("getListCuadrillas", "001: Algo salio mal al obtener las Cuadrillas: " + e.getMessage());
			return  new ArrayList<>();
		}
	}

	@Override
	public boolean deleteGeocerca(GeocercaQVO geocercaQVO) {
		try {
			
			namedParameterJdbcTemplateA.update(geocercaQVO.getqClearFfmQueue(), 
					new MapSqlParameterSource("FCT_ID", geocercaQVO.getId()));
			 return true;
			
		}catch(Exception e) {
			mensajes.console("deleteGeocerca", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			return false;
		}		
	}
	//Metodo masivo
	@Override
	public boolean saveAllOTQ(List<FFM_QUEUE_THEORY> listFFM_QUEUE_THEORY, String qInsertFfmQueue) {
		try {
			
			SqlParameterSource[] batchArgs = SqlParameterSourceUtils.createBatch(listFFM_QUEUE_THEORY.toArray());
			namedParameterJdbcTemplateA.batchUpdate(qInsertFfmQueue,batchArgs);
	 
			 return true;
			
		}catch(Exception e) {
			mensajes.console("lockUnlock", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			return false;
		}
	}
	//Metodo presenta lentitud en persistencia
	/*@Override
	public boolean saveAllOTQ(List<FFM_QUEUE_THEORY> listFFM_QUEUE_THEORY, String qInsertFfmQueue) {
		try {
			
			for (FFM_QUEUE_THEORY ffm_QUEUE_THEORY : listFFM_QUEUE_THEORY) {
				SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplateA)
		                .withTableName("FFM_QUEUE_THEORY").usingGeneratedKeyColumns("INDICE");
				 SqlParameterSource parameters = new BeanPropertySqlParameterSource(ffm_QUEUE_THEORY);
				 simpleJdbcInsert.execute(parameters);
			}
			
		}catch(Exception e) {
			mensajes.console("lockUnlock", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			return false;
		}
		return false;
	}*/
	
	/*@Override
	public boolean saveAllOTQ(List<FFM_QUEUE_THEORY> listFFM_QUEUE_THEORY, String qInsertFfmQueue) {
		try {
			
			for (FFM_QUEUE_THEORY otvo : listFFM_QUEUE_THEORY) {
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("EOT_ID", otvo.getFOT_ID());
				parametros.put("TIPO", otvo.getTIPO());
				parametros.put("ESO_OS", otvo.getFSO_OS());
				parametros.put("HORA_INICIO", otvo.getHORA_INICIO());
				parametros.put("HORA_FIN", otvo.getHORA_FIN());
				parametros.put("SEMAFORO", otvo.getSEMAFORO());
				parametros.put("DISTRITO", otvo.getGEOCERCA());
				parametros.put("EOT_CONFIRMATION", otvo.getFOT_CONFIRMATION());
				parametros.put("ECT_ID", otvo.getFCT_ID());
				parametros.put("TURNO", otvo.getTURNO());
				String query  = "INSERT INTO FFM_QUEUE_THEORY (\n" + 
						"	EOT_ID,\n" + 
						"	TIPO,\n" + 
						"	ESO_OS,\n" + 
						"	HORA_INICIO,\n" + 
						"	HORA_FIN,\n" + 
						"	SEMAFORO,\n" + 
						"	DISTRITO,\n" + 
						"	EOT_CONFIRMATION,\n" + 
						"	ECT_ID,\n" + 
						"	TURNO\n" + 
						")\n" + 
						"VALUES\n" + 
						"	(:EOT_ID ,:TIPO,:ESO_OS,:HORA_INICIO,:HORA_FIN,:SEMAFORO,:DISTRITO,:EOT_CONFIRMATION,:ECT_ID,:TURNO)";	
				
				return namedParameterJdbcTemplateA.update(query, 
						new MapSqlParameterSource(parametros)) == 1;
			}
			
		}catch(Exception e) {
			mensajes.console("lockUnlock", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			return false;
		}
		return false;
	}*/

	@Override
	public List<TurnoVO> findAllFfmCatTurn(String qGetTurn) {
		try {
			return jdbcTemplateA.query(qGetTurn, new RowMapper<TurnoVO>() {
				
				@Override
				public TurnoVO mapRow(ResultSet rs, int rowNum) throws SQLException {
					TurnoVO turnoVO = new TurnoVO();
					turnoVO.setFctuId(rs.getInt(1));
					turnoVO.setFctuDescription(rs.getString(2));
					turnoVO.setFctuHoraInicio(rs.getString(3));
					turnoVO.setFctuHoraFin(rs.getString(4));
					turnoVO.setFctuActivo(rs.getInt(5));
					return turnoVO;
				}				
			});
		}catch(Exception e) {
			mensajes.console("findAllFfmCatTurn", "Algo salio mal al obtener los turnos: " + e.getMessage());
			return null;
		}
	}

	@Override
	public String getHourOTAssign(int id, String getqHourOtAssign) {
		try {
						
			return namedParameterJdbcTemplateA.queryForObject(getqHourOtAssign, new MapSqlParameterSource("FCW_FCT_ID", id),
					new RowMapper<String>() {
						@Override
						public String mapRow(ResultSet rs, int arg1) throws SQLException {
							return rs.getString(1);							
						}
					});
		}catch(Exception e) {
			if(e.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
				mensajes.console("getHourOTAssign", "No hay OTs asignadas, la queue se formara con la asignacion inicial");
			}else {
				mensajes.console("getHourOTAssign", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			}			
			return "";
		}
	}

	/*@Override
	public void saveFAE(FFM_AA_EVENTOS fae) {
		try {
			SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(jdbcTemplateA)
	                .withTableName("FFM_AA_EVENTOS").usingGeneratedKeyColumns("ID");
			 SqlParameterSource parameters = new BeanPropertySqlParameterSource(fae);
			 simpleJdbcInsert.execute(parameters);	
		}catch(Exception e) {
			mensajes.console("save", "Algo salio mal, exception: "+ e.getMessage());
		}	
	}*/
	
	@Override
	public void saveFAE(List<FFM_AA_EVENTOS> listFae, String q) {
		try {			 
			 SqlParameterSource[] batchArgs = SqlParameterSourceUtils.createBatch(listFae.toArray());
				namedParameterJdbcTemplateA.batchUpdate(q,batchArgs);	 
			 
		}catch(Exception e) {
			mensajes.console("save", "Algo salio mal, exception: "+ e.getMessage());
		}	
	}
	
	private JornadaVO getJornada(String getqJornada, int ffoId) {
		try {
			
			return namedParameterJdbcTemplateA.queryForObject(getqJornada, new MapSqlParameterSource("FFO_ID", ffoId),
					new RowMapper<JornadaVO>() {
						@Override
						public JornadaVO mapRow(ResultSet rs, int arg1) throws SQLException {
							
							JornadaVO jornadaVO = new JornadaVO();
							
							jornadaVO.setFwhtFfoId(rs.getInt(1));
							jornadaVO.setlInicia(rs.getString(2));
							jornadaVO.setlTermina(rs.getString(3));
							jornadaVO.setmInicia(rs.getString(4));
							jornadaVO.setmTermina(rs.getString(5));
							jornadaVO.setwInicia(rs.getString(6));
							jornadaVO.setwTermina(rs.getString(7));
							jornadaVO.setjInicia(rs.getString(8));
							jornadaVO.setjTermina(rs.getString(9));
							jornadaVO.setvInicia(rs.getString(10));
							jornadaVO.setvTermina(rs.getString(11));
							jornadaVO.setsInicia(rs.getString(12));
							jornadaVO.setsTermina(rs.getString(13));
							jornadaVO.setdInicia(rs.getString(14));
							jornadaVO.setdTermina(rs.getString(15));
									
							return jornadaVO;							
						}
					});
		}catch(Exception e) {
			
			if(e.getMessage().equals("Incorrect result size: expected 1, actual 0")) {
				mensajes.console("getJornada", "002 Falla en los datos. No se encontraron Jornadas para el tecnico: "+ ffoId);
			}else {
				mensajes.console("getJornada", "002 Falla en los datos. Algo salio mal, exception: "+ e.getMessage());
			}
			return new JornadaVO();
		}
	}
}
