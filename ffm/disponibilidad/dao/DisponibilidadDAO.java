package ffm.disponibilidad.dao;

import java.util.List;

import ffm.disponibilidad.entidades.FFM_AA_EVENTOS;
import ffm.disponibilidad.entidades.FFM_QUEUE_THEORY;
import ffm.disponibilidad.vo.ArgumentosVO;
import ffm.disponibilidad.vo.CuadrillasVO;
import ffm.disponibilidad.vo.GeocercaQVO;
import ffm.disponibilidad.vo.GeocercaVO;
import ffm.disponibilidad.vo.OTVO;
import ffm.disponibilidad.vo.ParametrosVO;
import ffm.disponibilidad.vo.TurnoVO;

public interface DisponibilidadDAO {

	public List<ParametrosVO> getParameters(ArgumentosVO argumentsVO);

	public void getTimeZone(String query, GeocercaVO geocerca, GeocercaQVO geocercaQVO);

	public Integer exeLockUnlock(GeocercaVO geocercaVO, String qGetSemaforo);

	public int getResetParameters(ArgumentosVO argumentsVO);

	public int[] validateLock(GeocercaVO geocerca, String queryValidateLock);

	public boolean lockUnlock(Integer semaforo, GeocercaVO geocerca, int[] validateLock, String[] queryLockUnlock);

	public void updateResetParameters(String queryUpdateResetParams, ArgumentosVO argumentosVO);

	public List<OTVO> getListOTs(GeocercaQVO geocercaQVO);

	public List<CuadrillasVO> getListCuadrillas(GeocercaQVO geocercaQVO);

	public boolean saveAllOTQ(List<FFM_QUEUE_THEORY> listFFM_QUEUE_THEORY, String qInsertFfmQueue);

	public boolean deleteGeocerca(GeocercaQVO geocercaQVO);

	public List<TurnoVO> findAllFfmCatTurn(String qGetTurn);

	public String getHourOTAssign(int id, String getqHourOtAssign);

	public void saveFAE(List<FFM_AA_EVENTOS> listFFM_AA_EVENTOS, String string);
	
	//public void saveFAE(FFM_AA_EVENTOS fae);

	//public List<OTVO> getListOTsEmp(GeocercaQVO geocercaQVO);

}
