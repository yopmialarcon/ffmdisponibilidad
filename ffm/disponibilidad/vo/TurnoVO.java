package ffm.disponibilidad.vo;

public class TurnoVO {

	private int fctuId;
	
	private String fctuDescription;
	
	private String fctuHoraInicio;
	
	private String fctuHoraFin;
	
	private int fctuActivo;
	
	private float bolsaMinutos;
	
	private float porcentajeBolsaMinutos;

	public TurnoVO() {
		
	}

	public TurnoVO(int fctuId, String fctuDescription, String fctuHoraInicio, String fctuHoraFin, int fctuActivo,
			float bolsaMinutos, float porcentajeBolsaMinutos) {
		super();
		this.fctuId = fctuId;
		this.fctuDescription = fctuDescription;
		this.fctuHoraInicio = fctuHoraInicio;
		this.fctuHoraFin = fctuHoraFin;
		this.fctuActivo = fctuActivo;
		this.bolsaMinutos = bolsaMinutos;
		this.porcentajeBolsaMinutos = porcentajeBolsaMinutos;
	}

	public int getFctuId() {
		return fctuId;
	}

	public void setFctuId(int fctuId) {
		this.fctuId = fctuId;
	}

	public String getFctuDescription() {
		return fctuDescription;
	}

	public void setFctuDescription(String fctuDescription) {
		this.fctuDescription = fctuDescription;
	}

	public String getFctuHoraInicio() {
		return fctuHoraInicio;
	}

	public void setFctuHoraInicio(String fctuHoraInicio) {
		this.fctuHoraInicio = fctuHoraInicio;
	}

	public String getFctuHoraFin() {
		return fctuHoraFin;
	}

	public void setFctuHoraFin(String fctuHoraFin) {
		this.fctuHoraFin = fctuHoraFin;
	}

	public int getFctuActivo() {
		return fctuActivo;
	}

	public void setFctuActivo(int fctuActivo) {
		this.fctuActivo = fctuActivo;
	}

	public float getBolsaMinutos() {
		return bolsaMinutos;
	}

	public void setBolsaMinutos(float bolsaMinutos) {
		this.bolsaMinutos = bolsaMinutos;
	}

	public float getPorcentajeBolsaMinutos() {
		return porcentajeBolsaMinutos;
	}

	public void setPorcentajeBolsaMinutos(float porcentajeBolsaMinutos) {
		this.porcentajeBolsaMinutos = porcentajeBolsaMinutos;
	}

	@Override
	public String toString() {
		return "TurnoVO [fctuId=" + fctuId + ", fctuDescription=" + fctuDescription + ", fctuHoraInicio="
				+ fctuHoraInicio + ", fctuHoraFin=" + fctuHoraFin + ", fctuActivo=" + fctuActivo + ", bolsaMinutos="
				+ bolsaMinutos + ", porcentajeBolsaMinutos=" + porcentajeBolsaMinutos + "]";
	}
	
}
