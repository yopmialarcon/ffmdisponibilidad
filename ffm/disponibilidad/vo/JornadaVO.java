package ffm.disponibilidad.vo;

public class JornadaVO {

	private int fwhtFfoId;

	private String lInicia;

	private String lTermina;

	private String mInicia;

	private String mTermina;

	private String wInicia;

	private String wTermina;

	private String jInicia;

	private String jTermina;

	private String vInicia;

	private String vTermina;

	private String sInicia;

	private String sTermina;

	private String dInicia;

	private String dTermina;

	public JornadaVO() {
		
	}

	public JornadaVO(int fwhtFfoId, String lInicia, String lTermina, String mInicia, String mTermina, String wInicia,
			String wTermina, String jInicia, String jTermina, String vInicia, String vTermina, String sInicia,
			String sTermina, String dInicia, String dTermina) {
		super();
		this.fwhtFfoId = fwhtFfoId;
		this.lInicia = lInicia;
		this.lTermina = lTermina;
		this.mInicia = mInicia;
		this.mTermina = mTermina;
		this.wInicia = wInicia;
		this.wTermina = wTermina;
		this.jInicia = jInicia;
		this.jTermina = jTermina;
		this.vInicia = vInicia;
		this.vTermina = vTermina;
		this.sInicia = sInicia;
		this.sTermina = sTermina;
		this.dInicia = dInicia;
		this.dTermina = dTermina;
	}

	public int getFwhtFfoId() {
		return fwhtFfoId;
	}

	public void setFwhtFfoId(int fwhtFfoId) {
		this.fwhtFfoId = fwhtFfoId;
	}

	public String getlInicia() {
		return lInicia;
	}

	public void setlInicia(String lInicia) {
		this.lInicia = lInicia;
	}

	public String getlTermina() {
		return lTermina;
	}

	public void setlTermina(String lTermina) {
		this.lTermina = lTermina;
	}

	public String getmInicia() {
		return mInicia;
	}

	public void setmInicia(String mInicia) {
		this.mInicia = mInicia;
	}

	public String getmTermina() {
		return mTermina;
	}

	public void setmTermina(String mTermina) {
		this.mTermina = mTermina;
	}

	public String getwInicia() {
		return wInicia;
	}

	public void setwInicia(String wInicia) {
		this.wInicia = wInicia;
	}

	public String getwTermina() {
		return wTermina;
	}

	public void setwTermina(String wTermina) {
		this.wTermina = wTermina;
	}

	public String getjInicia() {
		return jInicia;
	}

	public void setjInicia(String jInicia) {
		this.jInicia = jInicia;
	}

	public String getjTermina() {
		return jTermina;
	}

	public void setjTermina(String jTermina) {
		this.jTermina = jTermina;
	}

	public String getvInicia() {
		return vInicia;
	}

	public void setvInicia(String vInicia) {
		this.vInicia = vInicia;
	}

	public String getvTermina() {
		return vTermina;
	}

	public void setvTermina(String vTermina) {
		this.vTermina = vTermina;
	}

	public String getsInicia() {
		return sInicia;
	}

	public void setsInicia(String sInicia) {
		this.sInicia = sInicia;
	}

	public String getsTermina() {
		return sTermina;
	}

	public void setsTermina(String sTermina) {
		this.sTermina = sTermina;
	}

	public String getdInicia() {
		return dInicia;
	}

	public void setdInicia(String dInicia) {
		this.dInicia = dInicia;
	}

	public String getdTermina() {
		return dTermina;
	}

	public void setdTermina(String dTermina) {
		this.dTermina = dTermina;
	}
	
}
