package ffm.disponibilidad.vo;

public class ParametrosVO {
	private int fapId;
	private Long fapStatus;
	private String fapValor;
	private boolean isGeocerca;
	private String description;

	public ParametrosVO() {
	}

	public ParametrosVO(int fapId, Long fapStatus, String fapValor, boolean isGeocerca, String description) {
		super();
		this.fapId = fapId;
		this.fapStatus = fapStatus;
		this.fapValor = fapValor;
		this.isGeocerca = isGeocerca;
		this.description = description;
	}

	public int getFapId() {
		return fapId;
	}

	public void setFapId(int fapId) {
		this.fapId = fapId;
	}

	public Long getFapStatus() {
		return fapStatus;
	}

	public void setFapStatus(Long fapStatus) {
		this.fapStatus = fapStatus;
	}

	public String getFapValor() {
		return fapValor;
	}

	public void setFapValor(String fapValor) {
		this.fapValor = fapValor;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isGeocerca() {
		return isGeocerca;
	}

	public void setGeocerca(boolean isGeocerca) {
		this.isGeocerca = isGeocerca;
	}

	@Override
	public String toString() {
		return "ParametrosVO [fapId=" + fapId + ", fapStatus=" + fapStatus + ", fapValor=" + fapValor + ", isGeocerca="
				+ isGeocerca + ", description=" + description + "]";
	}

}
