package ffm.disponibilidad.vo;

public class CuadrillasVO {
	
	private int ffoId;
	
	private double lat;
	
	private double len;
	
	private int fcopeId;
	
	private int fctId;
	
	private String fctIds;
	
	private JornadaVO jornadaVO;
	
	private float minutosRestantes;

	public CuadrillasVO() {
		
	}

	public CuadrillasVO(int ffoId, double lat, double len, int fcopeId, int fctId, String fctIds, JornadaVO jornadaVO,
			float minutosRestantes) {
		super();
		this.ffoId = ffoId;
		this.lat = lat;
		this.len = len;
		this.fcopeId = fcopeId;
		this.fctId = fctId;
		this.fctIds = fctIds;
		this.jornadaVO = jornadaVO;
		this.minutosRestantes = minutosRestantes;
	}

	public int getFfoId() {
		return ffoId;
	}

	public void setFfoId(int ffoId) {
		this.ffoId = ffoId;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLen() {
		return len;
	}

	public void setLen(double len) {
		this.len = len;
	}

	public int getFcopeId() {
		return fcopeId;
	}

	public void setFcopeId(int fcopeId) {
		this.fcopeId = fcopeId;
	}

	public int getFctId() {
		return fctId;
	}

	public void setFctId(int fctId) {
		this.fctId = fctId;
	}

	public String getFctIds() {
		return fctIds;
	}

	public void setFctIds(String fctIds) {
		this.fctIds = fctIds;
	}

	public JornadaVO getJornadaVO() {
		return jornadaVO;
	}

	public void setJornadaVO(JornadaVO jornadaVO) {
		this.jornadaVO = jornadaVO;
	}

	public float getMinutosRestantes() {
		return minutosRestantes;
	}

	public void setMinutosRestantes(float minutosRestantes) {
		this.minutosRestantes = minutosRestantes;
	}
}
