package ffm.disponibilidad.vo;

import java.util.Arrays;

public class ArgumentosVO {
	private String application;
	private String ambient;
	private String os;	
	private int configuracion;
	private byte[] user;
	private byte[] paswd;
	private byte[] stringConnection;
	
	public ArgumentosVO() {
	}

	public ArgumentosVO(String application, String ambient, String os, int configuracion, byte[] user, byte[] paswd,
			byte[] stringConnection) {
		super();
		this.application = application;
		this.ambient = ambient;
		this.os = os;
		this.configuracion = configuracion;
		this.user = user;
		this.paswd = paswd;
		this.stringConnection = stringConnection;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getAmbient() {
		return ambient;
	}

	public void setAmbient(String ambient) {
		this.ambient = ambient;
	}

	public String getOs() {
		return os;
	}

	public void setOs(String os) {
		this.os = os;
	}

	public int getConfiguracion() {
		return configuracion;
	}

	public void setConfiguracion(int configuracion) {
		this.configuracion = configuracion;
	}

	public byte[] getUser() {
		return user;
	}

	public void setUser(byte[] user) {
		this.user = user;
	}

	public byte[] getPaswd() {
		return paswd;
	}

	public void setPaswd(byte[] paswd) {
		this.paswd = paswd;
	}

	public byte[] getStringConnection() {
		return stringConnection;
	}

	public void setStringConnection(byte[] stringConnection) {
		this.stringConnection = stringConnection;
	}

	@Override
	public String toString() {
		return "ArgumentosVO [application=" + application + ", ambient=" + ambient + ", os=" + os + ", configuracion="
				+ configuracion + ", user=" + Arrays.toString(user) + ", paswd=" + Arrays.toString(paswd)
				+ ", stringConnection=" + Arrays.toString(stringConnection) + "]";
	}
		
}
