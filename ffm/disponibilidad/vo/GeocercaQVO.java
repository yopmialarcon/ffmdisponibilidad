package ffm.disponibilidad.vo;

import java.util.List;

public class GeocercaQVO {
	
	private int id;
	
	private int google;
	
	private int timeBySkill;
	
	private int eneable;
	
	private String skills;
	
	private String startTime;
	
	private String endTime;
	
	private int tolerance;
	
	private int speed;
	
	private int turn;
	
	private int minutesWaitIVR;
	
	private int oTsOld;
	
	private int oTsPrematures;
	
	private String timeZone;
	
	private int hoursDifference;
	
	private String qOTs;
	
	private String qCuadrillas;
	
	private String qJornada;
	
	private String qClearFfmQueue;
	
	private String qHourOtAssign;
	
	private String qInsertFfmQueue;
	
	private String qInsertFAE;
	
	private String skillsTiempoPromedio;
	
	private float bolsaMinutosCuadrillas;
	
	private float bolsaMinutosTurno;
	
	private List<OTVO> listOTs;
	
	private List<CuadrillasVO> listCuadrillasVO;

	public GeocercaQVO() {
		
	}

	public GeocercaQVO(int id, int google, int timeBySkill, int eneable, String skills, String startTime,
			String endTime, int tolerance, int speed, int turn, int minutesWaitIVR, int oTsOld, int oTsPrematures,
			String timeZone, int hoursDifference, String qOTs, String qCuadrillas, String qJornada,
			String qClearFfmQueue, String qHourOtAssign, String qInsertFfmQueue, String qInsertFAE,
			String skillsTiempoPromedio, float bolsaMinutosCuadrillas, float bolsaMinutosTurno, List<OTVO> listOTs,
			List<CuadrillasVO> listCuadrillasVO) {
		super();
		this.id = id;
		this.google = google;
		this.timeBySkill = timeBySkill;
		this.eneable = eneable;
		this.skills = skills;
		this.startTime = startTime;
		this.endTime = endTime;
		this.tolerance = tolerance;
		this.speed = speed;
		this.turn = turn;
		this.minutesWaitIVR = minutesWaitIVR;
		this.oTsOld = oTsOld;
		this.oTsPrematures = oTsPrematures;
		this.timeZone = timeZone;
		this.hoursDifference = hoursDifference;
		this.qOTs = qOTs;
		this.qCuadrillas = qCuadrillas;
		this.qJornada = qJornada;
		this.qClearFfmQueue = qClearFfmQueue;
		this.qHourOtAssign = qHourOtAssign;
		this.qInsertFfmQueue = qInsertFfmQueue;
		this.qInsertFAE = qInsertFAE;
		this.skillsTiempoPromedio = skillsTiempoPromedio;
		this.bolsaMinutosCuadrillas = bolsaMinutosCuadrillas;
		this.bolsaMinutosTurno = bolsaMinutosTurno;
		this.listOTs = listOTs;
		this.listCuadrillasVO = listCuadrillasVO;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getGoogle() {
		return google;
	}

	public void setGoogle(int google) {
		this.google = google;
	}

	public int getTimeBySkill() {
		return timeBySkill;
	}

	public void setTimeBySkill(int timeBySkill) {
		this.timeBySkill = timeBySkill;
	}

	public int getEneable() {
		return eneable;
	}

	public void setEneable(int eneable) {
		this.eneable = eneable;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getTolerance() {
		return tolerance;
	}

	public void setTolerance(int tolerance) {
		this.tolerance = tolerance;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		this.turn = turn;
	}

	public int getMinutesWaitIVR() {
		return minutesWaitIVR;
	}

	public void setMinutesWaitIVR(int minutesWaitIVR) {
		this.minutesWaitIVR = minutesWaitIVR;
	}

	public int getoTsOld() {
		return oTsOld;
	}

	public void setoTsOld(int oTsOld) {
		this.oTsOld = oTsOld;
	}

	public int getoTsPrematures() {
		return oTsPrematures;
	}

	public void setoTsPrematures(int oTsPrematures) {
		this.oTsPrematures = oTsPrematures;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public int getHoursDifference() {
		return hoursDifference;
	}

	public void setHoursDifference(int hoursDifference) {
		this.hoursDifference = hoursDifference;
	}

	public String getqOTs() {
		return qOTs;
	}

	public void setqOTs(String qOTs) {
		this.qOTs = qOTs;
	}

	public String getqCuadrillas() {
		return qCuadrillas;
	}

	public void setqCuadrillas(String qCuadrillas) {
		this.qCuadrillas = qCuadrillas;
	}

	public String getqJornada() {
		return qJornada;
	}

	public void setqJornada(String qJornada) {
		this.qJornada = qJornada;
	}

	public String getqClearFfmQueue() {
		return qClearFfmQueue;
	}

	public void setqClearFfmQueue(String qClearFfmQueue) {
		this.qClearFfmQueue = qClearFfmQueue;
	}

	public String getqHourOtAssign() {
		return qHourOtAssign;
	}

	public void setqHourOtAssign(String qHourOtAssign) {
		this.qHourOtAssign = qHourOtAssign;
	}

	public String getqInsertFfmQueue() {
		return qInsertFfmQueue;
	}

	public void setqInsertFfmQueue(String qInsertFfmQueue) {
		this.qInsertFfmQueue = qInsertFfmQueue;
	}

	public String getqInsertFAE() {
		return qInsertFAE;
	}

	public void setqInsertFAE(String qInsertFAE) {
		this.qInsertFAE = qInsertFAE;
	}

	public String getSkillsTiempoPromedio() {
		return skillsTiempoPromedio;
	}

	public void setSkillsTiempoPromedio(String skillsTiempoPromedio) {
		this.skillsTiempoPromedio = skillsTiempoPromedio;
	}

	public float getBolsaMinutosCuadrillas() {
		return bolsaMinutosCuadrillas;
	}

	public void setBolsaMinutosCuadrillas(float bolsaMinutosCuadrillas) {
		this.bolsaMinutosCuadrillas = bolsaMinutosCuadrillas;
	}

	public float getBolsaMinutosTurno() {
		return bolsaMinutosTurno;
	}

	public void setBolsaMinutosTurno(float bolsaMinutosTurno) {
		this.bolsaMinutosTurno = bolsaMinutosTurno;
	}

	public List<OTVO> getListOTs() {
		return listOTs;
	}

	public void setListOTs(List<OTVO> listOTs) {
		this.listOTs = listOTs;
	}

	public List<CuadrillasVO> getListCuadrillasVO() {
		return listCuadrillasVO;
	}

	public void setListCuadrillasVO(List<CuadrillasVO> listCuadrillasVO) {
		this.listCuadrillasVO = listCuadrillasVO;
	}

	@Override
	public String toString() {
		return "GeocercaQVO [id=" + id + ", google=" + google + ", timeBySkill=" + timeBySkill + ", eneable=" + eneable
				+ ", skills=" + skills + ", startTime=" + startTime + ", endTime=" + endTime + ", tolerance="
				+ tolerance + ", speed=" + speed + ", turn=" + turn + ", minutesWaitIVR=" + minutesWaitIVR + ", oTsOld="
				+ oTsOld + ", oTsPrematures=" + oTsPrematures + ", timeZone=" + timeZone + ", hoursDifference="
				+ hoursDifference + ", qOTs=" + qOTs + ", qCuadrillas=" + qCuadrillas + ", qJornada=" + qJornada
				+ ", qClearFfmQueue=" + qClearFfmQueue + ", qHourOtAssign=" + qHourOtAssign + ", qInsertFfmQueue="
				+ qInsertFfmQueue + ", qInsertFAE=" + qInsertFAE + ", skillsTiempoPromedio=" + skillsTiempoPromedio
				+ ", bolsaMinutosCuadrillas=" + bolsaMinutosCuadrillas + ", bolsaMinutosTurno=" + bolsaMinutosTurno
				+ ", listOTs=" + listOTs + ", listCuadrillasVO=" + listCuadrillasVO + "]";
	}
	
		
}
