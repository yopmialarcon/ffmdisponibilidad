package ffm.disponibilidad.vo;

import java.time.LocalDateTime;

public class OTVO {

	private int fotId;
	
	private int idGeo;
	
	private int vip;
	
	private int fotTicketsRaised;
	
	private String turnoInicia;
	
	private int fciIdSubTypeInter;
	
	private int fctId;
	
	private int fotConfirmation;
	
	private float timeAvg;
	
	private String fsoOs;
	
	private String fciDescription;
	
	private int fctuId;
	
	private LocalDateTime dateStart;
	
	private LocalDateTime dateEnd;
	
	private String fotHour;
	
	private String zone;
	
	private int flag = 1;
	
	private int semaforo;
	
	private int ectId;

	public OTVO() {
		
	}

	public OTVO(int fotId, int idGeo, int vip, int fotTicketsRaised, String turnoInicia, int fciIdSubTypeInter,
			int fctId, int fotConfirmation, float timeAvg, String fsoOs, String fciDescription, int fctuId,
			LocalDateTime dateStart, LocalDateTime dateEnd, String fotHour, String zone, int flag, int semaforo,
			int eciId, int ectId) {
		super();
		this.fotId = fotId;
		this.idGeo = idGeo;
		this.vip = vip;
		this.fotTicketsRaised = fotTicketsRaised;
		this.turnoInicia = turnoInicia;
		this.fciIdSubTypeInter = fciIdSubTypeInter;
		this.fctId = fctId;
		this.fotConfirmation = fotConfirmation;
		this.timeAvg = timeAvg;
		this.fsoOs = fsoOs;
		this.fciDescription = fciDescription;
		this.fctuId = fctuId;
		this.dateStart = dateStart;
		this.dateEnd = dateEnd;
		this.fotHour = fotHour;
		this.zone = zone;
		this.flag = flag;
		this.semaforo = semaforo;
		this.ectId = ectId;
	}

	public int getFotId() {
		return fotId;
	}

	public void setFotId(int fotId) {
		this.fotId = fotId;
	}

	public int getIdGeo() {
		return idGeo;
	}

	public void setIdGeo(int idGeo) {
		this.idGeo = idGeo;
	}

	public int getVip() {
		return vip;
	}

	public void setVip(int vip) {
		this.vip = vip;
	}

	public int getFotTicketsRaised() {
		return fotTicketsRaised;
	}

	public void setFotTicketsRaised(int fotTicketsRaised) {
		this.fotTicketsRaised = fotTicketsRaised;
	}

	public String getTurnoInicia() {
		return turnoInicia;
	}

	public void setTurnoInicia(String turnoInicia) {
		this.turnoInicia = turnoInicia;
	}

	public int getFciIdSubTypeInter() {
		return fciIdSubTypeInter;
	}

	public void setFciIdSubTypeInter(int fciIdSubTypeInter) {
		this.fciIdSubTypeInter = fciIdSubTypeInter;
	}

	public int getFctId() {
		return fctId;
	}

	public void setFctId(int fctId) {
		this.fctId = fctId;
	}

	public int getFotConfirmation() {
		return fotConfirmation;
	}

	public void setFotConfirmation(int fotConfirmation) {
		this.fotConfirmation = fotConfirmation;
	}

	public float getTimeAvg() {
		return timeAvg;
	}

	public void setTimeAvg(float timeAvg) {
		this.timeAvg = timeAvg;
	}

	public String getFsoOs() {
		return fsoOs;
	}

	public void setFsoOs(String fsoOs) {
		this.fsoOs = fsoOs;
	}

	public String getFciDescription() {
		return fciDescription;
	}

	public void setFciDescription(String fciDescription) {
		this.fciDescription = fciDescription;
	}

	public int getFctuId() {
		return fctuId;
	}

	public void setFctuId(int fctuId) {
		this.fctuId = fctuId;
	}

	public LocalDateTime getDateStart() {
		return dateStart;
	}

	public void setDateStart(LocalDateTime dateStart) {
		this.dateStart = dateStart;
	}

	public LocalDateTime getDateEnd() {
		return dateEnd;
	}

	public void setDateEnd(LocalDateTime dateEnd) {
		this.dateEnd = dateEnd;
	}

	public String getFotHour() {
		return fotHour;
	}

	public void setFotHour(String fotHour) {
		this.fotHour = fotHour;
	}

	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}

	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public int getSemaforo() {
		return semaforo;
	}

	public void setSemaforo(int semaforo) {
		this.semaforo = semaforo;
	}

	public int getEctId() {
		return ectId;
	}

	public void setEctId(int ectId) {
		this.ectId = ectId;
	}

	@Override
	public String toString() {
		return "OTVO [fotId=" + fotId + ", idGeo=" + idGeo + ", vip=" + vip + ", fotTicketsRaised=" + fotTicketsRaised
				+ ", turnoInicia=" + turnoInicia + ", fciIdSubTypeInter=" + fciIdSubTypeInter + ", fctId=" + fctId
				+ ", fotConfirmation=" + fotConfirmation + ", timeAvg=" + timeAvg + ", fsoOs=" + fsoOs
				+ ", fciDescription=" + fciDescription + ", fctuId=" + fctuId + ", dateStart=" + dateStart
				+ ", dateEnd=" + dateEnd + ", fotHour=" + fotHour + ", zone=" + zone + ", flag=" + flag + ", semaforo="
				+ semaforo + ", ectId=" + ectId + "]";
	}

	

	
	
}
