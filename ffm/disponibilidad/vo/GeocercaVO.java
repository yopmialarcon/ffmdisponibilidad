package ffm.disponibilidad.vo;

public class GeocercaVO {
	private int id;
	private boolean enabled;
	private String startTime;
	private String endTime;
	private int[] skills;
	private String timeZone;
	private int hoursDifference;
	
	public GeocercaVO() {
	}

	public GeocercaVO(int id, boolean enabled, String startTime, String endTime, int[] skills, String timeZone,
			int hoursDifference) {
		super();
		this.id = id;
		this.enabled = enabled;
		this.startTime = startTime;
		this.endTime = endTime;
		this.skills = skills;
		this.timeZone = timeZone;
		this.hoursDifference = hoursDifference;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int[] getSkills() {
		return skills;
	}

	public void setSkills(int[] skills) {
		this.skills = skills;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public int getHoursDifference() {
		return hoursDifference;
	}

	public void setHoursDifference(int hoursDifference) {
		this.hoursDifference = hoursDifference;
	}
	
}
