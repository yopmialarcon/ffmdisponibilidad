package ffm.disponibilidad.entidades;

import java.io.Serializable;
import java.time.LocalDateTime;

public class FFM_QUEUE_THEORY implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -654828379517773196L;

	private int INDICE;
	
	private int FLAG;
	
	private int FOT_ID;
	
	private String TIPO;
	
	private String FSO_OS;
	
	private LocalDateTime HORA_INICIO;
	
	private LocalDateTime HORA_FIN;
	
	private int MINUTOS;
	
	private int MINUTOS_ACUMULADOS;
	
	private int SEMAFORO;
	
	private String GEOCERCA;
	
	private int FOT_CONFIRMATION;
	
	private int FCT_ID;
	
	private int CONTROLADOR;
	
	private int TURNO;
	
	private int ECI_ID;
	
	private int ECT_ID;

	public FFM_QUEUE_THEORY() {
	}

	public FFM_QUEUE_THEORY(int iNDICE, int fLAG, int fOT_ID, String tIPO, String fSO_OS, LocalDateTime hORA_INICIO,
			LocalDateTime hORA_FIN, int mINUTOS, int mINUTOS_ACUMULADOS, int sEMAFORO, String gEOCERCA,
			int fOT_CONFIRMATION, int fCT_ID, int cONTROLADOR, int tURNO, int eCI_ID, int eCT_ID) {
		super();
		INDICE = iNDICE;
		FLAG = fLAG;
		FOT_ID = fOT_ID;
		TIPO = tIPO;
		FSO_OS = fSO_OS;
		HORA_INICIO = hORA_INICIO;
		HORA_FIN = hORA_FIN;
		MINUTOS = mINUTOS;
		MINUTOS_ACUMULADOS = mINUTOS_ACUMULADOS;
		SEMAFORO = sEMAFORO;
		GEOCERCA = gEOCERCA;
		FOT_CONFIRMATION = fOT_CONFIRMATION;
		FCT_ID = fCT_ID;
		CONTROLADOR = cONTROLADOR;
		TURNO = tURNO;
		ECI_ID = eCI_ID;
		ECT_ID = eCT_ID;
	}

	public int getINDICE() {
		return INDICE;
	}

	public void setINDICE(int iNDICE) {
		INDICE = iNDICE;
	}

	public int getFLAG() {
		return FLAG;
	}

	public void setFLAG(int fLAG) {
		FLAG = fLAG;
	}

	public int getFOT_ID() {
		return FOT_ID;
	}

	public void setFOT_ID(int fOT_ID) {
		FOT_ID = fOT_ID;
	}

	public String getTIPO() {
		return TIPO;
	}

	public void setTIPO(String tIPO) {
		TIPO = tIPO;
	}

	public String getFSO_OS() {
		return FSO_OS;
	}

	public void setFSO_OS(String fSO_OS) {
		FSO_OS = fSO_OS;
	}

	public LocalDateTime getHORA_INICIO() {
		return HORA_INICIO;
	}

	public void setHORA_INICIO(LocalDateTime hORA_INICIO) {
		HORA_INICIO = hORA_INICIO;
	}

	public LocalDateTime getHORA_FIN() {
		return HORA_FIN;
	}

	public void setHORA_FIN(LocalDateTime hORA_FIN) {
		HORA_FIN = hORA_FIN;
	}

	public int getMINUTOS() {
		return MINUTOS;
	}

	public void setMINUTOS(int mINUTOS) {
		MINUTOS = mINUTOS;
	}

	public int getMINUTOS_ACUMULADOS() {
		return MINUTOS_ACUMULADOS;
	}

	public void setMINUTOS_ACUMULADOS(int mINUTOS_ACUMULADOS) {
		MINUTOS_ACUMULADOS = mINUTOS_ACUMULADOS;
	}

	public int getSEMAFORO() {
		return SEMAFORO;
	}

	public void setSEMAFORO(int sEMAFORO) {
		SEMAFORO = sEMAFORO;
	}

	public String getGEOCERCA() {
		return GEOCERCA;
	}

	public void setGEOCERCA(String gEOCERCA) {
		GEOCERCA = gEOCERCA;
	}

	public int getFOT_CONFIRMATION() {
		return FOT_CONFIRMATION;
	}

	public void setFOT_CONFIRMATION(int fOT_CONFIRMATION) {
		FOT_CONFIRMATION = fOT_CONFIRMATION;
	}

	public int getFCT_ID() {
		return FCT_ID;
	}

	public void setFCT_ID(int fCT_ID) {
		FCT_ID = fCT_ID;
	}

	public int getCONTROLADOR() {
		return CONTROLADOR;
	}

	public void setCONTROLADOR(int cONTROLADOR) {
		CONTROLADOR = cONTROLADOR;
	}

	public int getTURNO() {
		return TURNO;
	}

	public void setTURNO(int tURNO) {
		TURNO = tURNO;
	}

	public int getECI_ID() {
		return ECI_ID;
	}

	public void setECI_ID(int eCI_ID) {
		ECI_ID = eCI_ID;
	}

	public int getECT_ID() {
		return ECT_ID;
	}

	public void setECT_ID(int eCT_ID) {
		ECT_ID = eCT_ID;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "FFM_QUEUE_THEORY [INDICE=" + INDICE + ", FLAG=" + FLAG + ", FOT_ID=" + FOT_ID + ", TIPO=" + TIPO
				+ ", FSO_OS=" + FSO_OS + ", HORA_INICIO=" + HORA_INICIO + ", HORA_FIN=" + HORA_FIN + ", MINUTOS="
				+ MINUTOS + ", MINUTOS_ACUMULADOS=" + MINUTOS_ACUMULADOS + ", SEMAFORO=" + SEMAFORO + ", GEOCERCA="
				+ GEOCERCA + ", FOT_CONFIRMATION=" + FOT_CONFIRMATION + ", FCT_ID=" + FCT_ID + ", CONTROLADOR="
				+ CONTROLADOR + ", TURNO=" + TURNO + ", ECI_ID=" + ECI_ID + ", ECT_ID=" + ECT_ID + "]";
	}

	

}
