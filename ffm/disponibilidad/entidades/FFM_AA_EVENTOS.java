package ffm.disponibilidad.entidades;

import java.io.Serializable;
import java.time.LocalDateTime;

public class FFM_AA_EVENTOS implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4757452916228522144L;

	private int ID;
	
	private LocalDateTime FAE_TIMESTAMP;
	
	private String FAE_MODULO;
	
	private String FAE_PROCESO;
	
	private int FAE_CICLO;
	
	private int FAE_FFO_FCT_ID;
	
	private int FAE_CASO;
	
	private int FAE_FFO_ID;
	
	private int FAE_FFO_LATITUD;
	
	private int FAE_FFO_LONGITUD;
	
	private int FAE_FOT_ID;
	
	private int FAE_FOT_LATITUD;
	
	private int FAE_FOT_LONGITUD;
	
	private int FAE_FOT_FCT_ID;
	
	private int FAE_DISTANCIA;
	
	private int FAE_ESTATUS_ASIGNACION;
	
	private int FAE_ESTATUS_SOLICITUD;
	
	private String FAE_MENSAJE;
	
	private int FAE_CANTIDAD_DE_FOT;
	
	private int FAE_CANTIDAD_DE_FFO;
	
	private int FAE_FOT_FCI_ID;
	
	private String FAE_FFO_FCI_IDS;

	public FFM_AA_EVENTOS() {
		
	}

	public FFM_AA_EVENTOS(int iD, LocalDateTime fAE_TIMESTAMP, String fAE_MODULO, String fAE_PROCESO, int fAE_CICLO,
			int fAE_FFO_FCT_ID, int fAE_CASO, int fAE_FFO_ID, int fAE_FFO_LATITUD, int fAE_FFO_LONGITUD, int fAE_FOT_ID,
			int fAE_FOT_LATITUD, int fAE_FOT_LONGITUD, int fAE_FOT_FCT_ID, int fAE_DISTANCIA,
			int fAE_ESTATUS_ASIGNACION, int fAE_ESTATUS_SOLICITUD, String fAE_MENSAJE, int fAE_CANTIDAD_DE_FOT,
			int fAE_CANTIDAD_DE_FFO, int fAE_FOT_FCI_ID, String fAE_FFO_FCI_IDS) {
		super();
		ID = iD;
		FAE_TIMESTAMP = fAE_TIMESTAMP;
		FAE_MODULO = fAE_MODULO;
		FAE_PROCESO = fAE_PROCESO;
		FAE_CICLO = fAE_CICLO;
		FAE_FFO_FCT_ID = fAE_FFO_FCT_ID;
		FAE_CASO = fAE_CASO;
		FAE_FFO_ID = fAE_FFO_ID;
		FAE_FFO_LATITUD = fAE_FFO_LATITUD;
		FAE_FFO_LONGITUD = fAE_FFO_LONGITUD;
		FAE_FOT_ID = fAE_FOT_ID;
		FAE_FOT_LATITUD = fAE_FOT_LATITUD;
		FAE_FOT_LONGITUD = fAE_FOT_LONGITUD;
		FAE_FOT_FCT_ID = fAE_FOT_FCT_ID;
		FAE_DISTANCIA = fAE_DISTANCIA;
		FAE_ESTATUS_ASIGNACION = fAE_ESTATUS_ASIGNACION;
		FAE_ESTATUS_SOLICITUD = fAE_ESTATUS_SOLICITUD;
		FAE_MENSAJE = fAE_MENSAJE;
		FAE_CANTIDAD_DE_FOT = fAE_CANTIDAD_DE_FOT;
		FAE_CANTIDAD_DE_FFO = fAE_CANTIDAD_DE_FFO;
		FAE_FOT_FCI_ID = fAE_FOT_FCI_ID;
		FAE_FFO_FCI_IDS = fAE_FFO_FCI_IDS;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public LocalDateTime getFAE_TIMESTAMP() {
		return FAE_TIMESTAMP;
	}

	public void setFAE_TIMESTAMP(LocalDateTime fAE_TIMESTAMP) {
		FAE_TIMESTAMP = fAE_TIMESTAMP;
	}

	public String getFAE_MODULO() {
		return FAE_MODULO;
	}

	public void setFAE_MODULO(String fAE_MODULO) {
		FAE_MODULO = fAE_MODULO;
	}

	public String getFAE_PROCESO() {
		return FAE_PROCESO;
	}

	public void setFAE_PROCESO(String fAE_PROCESO) {
		FAE_PROCESO = fAE_PROCESO;
	}

	public int getFAE_CICLO() {
		return FAE_CICLO;
	}

	public void setFAE_CICLO(int fAE_CICLO) {
		FAE_CICLO = fAE_CICLO;
	}

	public int getFAE_FFO_FCT_ID() {
		return FAE_FFO_FCT_ID;
	}

	public void setFAE_FFO_FCT_ID(int fAE_FFO_FCT_ID) {
		FAE_FFO_FCT_ID = fAE_FFO_FCT_ID;
	}

	public int getFAE_CASO() {
		return FAE_CASO;
	}

	public void setFAE_CASO(int fAE_CASO) {
		FAE_CASO = fAE_CASO;
	}

	public int getFAE_FFO_ID() {
		return FAE_FFO_ID;
	}

	public void setFAE_FFO_ID(int fAE_FFO_ID) {
		FAE_FFO_ID = fAE_FFO_ID;
	}

	public int getFAE_FFO_LATITUD() {
		return FAE_FFO_LATITUD;
	}

	public void setFAE_FFO_LATITUD(int fAE_FFO_LATITUD) {
		FAE_FFO_LATITUD = fAE_FFO_LATITUD;
	}

	public int getFAE_FFO_LONGITUD() {
		return FAE_FFO_LONGITUD;
	}

	public void setFAE_FFO_LONGITUD(int fAE_FFO_LONGITUD) {
		FAE_FFO_LONGITUD = fAE_FFO_LONGITUD;
	}

	public int getFAE_FOT_ID() {
		return FAE_FOT_ID;
	}

	public void setFAE_FOT_ID(int fAE_FOT_ID) {
		FAE_FOT_ID = fAE_FOT_ID;
	}

	public int getFAE_FOT_LATITUD() {
		return FAE_FOT_LATITUD;
	}

	public void setFAE_FOT_LATITUD(int fAE_FOT_LATITUD) {
		FAE_FOT_LATITUD = fAE_FOT_LATITUD;
	}

	public int getFAE_FOT_LONGITUD() {
		return FAE_FOT_LONGITUD;
	}

	public void setFAE_FOT_LONGITUD(int fAE_FOT_LONGITUD) {
		FAE_FOT_LONGITUD = fAE_FOT_LONGITUD;
	}

	public int getFAE_FOT_FCT_ID() {
		return FAE_FOT_FCT_ID;
	}

	public void setFAE_FOT_FCT_ID(int fAE_FOT_FCT_ID) {
		FAE_FOT_FCT_ID = fAE_FOT_FCT_ID;
	}

	public int getFAE_DISTANCIA() {
		return FAE_DISTANCIA;
	}

	public void setFAE_DISTANCIA(int fAE_DISTANCIA) {
		FAE_DISTANCIA = fAE_DISTANCIA;
	}

	public int getFAE_ESTATUS_ASIGNACION() {
		return FAE_ESTATUS_ASIGNACION;
	}

	public void setFAE_ESTATUS_ASIGNACION(int fAE_ESTATUS_ASIGNACION) {
		FAE_ESTATUS_ASIGNACION = fAE_ESTATUS_ASIGNACION;
	}

	public int getFAE_ESTATUS_SOLICITUD() {
		return FAE_ESTATUS_SOLICITUD;
	}

	public void setFAE_ESTATUS_SOLICITUD(int fAE_ESTATUS_SOLICITUD) {
		FAE_ESTATUS_SOLICITUD = fAE_ESTATUS_SOLICITUD;
	}

	public String getFAE_MENSAJE() {
		return FAE_MENSAJE;
	}

	public void setFAE_MENSAJE(String fAE_MENSAJE) {
		FAE_MENSAJE = fAE_MENSAJE;
	}

	public int getFAE_CANTIDAD_DE_FOT() {
		return FAE_CANTIDAD_DE_FOT;
	}

	public void setFAE_CANTIDAD_DE_FOT(int fAE_CANTIDAD_DE_FOT) {
		FAE_CANTIDAD_DE_FOT = fAE_CANTIDAD_DE_FOT;
	}

	public int getFAE_CANTIDAD_DE_FFO() {
		return FAE_CANTIDAD_DE_FFO;
	}

	public void setFAE_CANTIDAD_DE_FFO(int fAE_CANTIDAD_DE_FFO) {
		FAE_CANTIDAD_DE_FFO = fAE_CANTIDAD_DE_FFO;
	}

	public int getFAE_FOT_FCI_ID() {
		return FAE_FOT_FCI_ID;
	}

	public void setFAE_FOT_FCI_ID(int fAE_FOT_FCI_ID) {
		FAE_FOT_FCI_ID = fAE_FOT_FCI_ID;
	}

	public String getFAE_FFO_FCI_IDS() {
		return FAE_FFO_FCI_IDS;
	}

	public void setFAE_FFO_FCI_IDS(String fAE_FFO_FCI_IDS) {
		FAE_FFO_FCI_IDS = fAE_FFO_FCI_IDS;
	}

	@Override
	public String toString() {
		return "EventosVO [ID=" + ID + ", FAE_TIMESTAMP=" + FAE_TIMESTAMP + ", FAE_MODULO=" + FAE_MODULO
				+ ", FAE_PROCESO=" + FAE_PROCESO + ", FAE_CICLO=" + FAE_CICLO + ", FAE_FFO_FCT_ID=" + FAE_FFO_FCT_ID
				+ ", FAE_CASO=" + FAE_CASO + ", FAE_FFO_ID=" + FAE_FFO_ID + ", FAE_FFO_LATITUD=" + FAE_FFO_LATITUD
				+ ", FAE_FFO_LONGITUD=" + FAE_FFO_LONGITUD + ", FAE_FOT_ID=" + FAE_FOT_ID + ", FAE_FOT_LATITUD="
				+ FAE_FOT_LATITUD + ", FAE_FOT_LONGITUD=" + FAE_FOT_LONGITUD + ", FAE_FOT_FCT_ID=" + FAE_FOT_FCT_ID
				+ ", FAE_DISTANCIA=" + FAE_DISTANCIA + ", FAE_ESTATUS_ASIGNACION=" + FAE_ESTATUS_ASIGNACION
				+ ", FAE_ESTATUS_SOLICITUD=" + FAE_ESTATUS_SOLICITUD + ", FAE_MENSAJE=" + FAE_MENSAJE
				+ ", FAE_CANTIDAD_DE_FOT=" + FAE_CANTIDAD_DE_FOT + ", FAE_CANTIDAD_DE_FFO=" + FAE_CANTIDAD_DE_FFO
				+ ", FAE_FOT_FCI_ID=" + FAE_FOT_FCI_ID + ", FAE_FFO_FCI_IDS=" + FAE_FFO_FCI_IDS + "]";
	}
	

}
